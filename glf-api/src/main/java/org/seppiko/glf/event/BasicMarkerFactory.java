/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.event;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.seppiko.glf.api.IMarkerFactory;
import org.seppiko.glf.api.Marker;

/**
 * Basic Marker Factory for Marker Factory interface default implementation
 *
 * @author Leonard Woo
 */
public class BasicMarkerFactory implements IMarkerFactory {

  private final ConcurrentMap<String, Marker> markerMap = new ConcurrentHashMap<>();

  @Override
  public Marker getMarker(String name) {
    if (name == null) {
      throw new IllegalArgumentException("Marker name cannot be null");
    }

    Marker marker = markerMap.get(name);
    if (marker == null) {
      marker = new BasicMarker(name);
      Marker oldMarker = markerMap.putIfAbsent(name, marker);
      if (oldMarker != null) {
        marker = oldMarker;
      }
    }
    return marker;
  }

  @Override
  public boolean exists(String name) {
    if (name == null) {
      return false;
    }
    return markerMap.containsKey(name);
  }

  @Override
  public boolean detachMarker(String name) {
    if (name == null) {
      return false;
    }
    return (markerMap.remove(name) != null);
  }

  @Override
  public Marker getDetachedMarker(String name) {
    return new BasicMarker(name);
  }
}
