/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.event;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import org.seppiko.glf.api.Marker;

/**
 * Basic Marker for default implementation
 *
 * @author Leonard Woo
 */
public class BasicMarker implements Marker {

  private String name;
  private List<Marker> referenceList = new CopyOnWriteArrayList<>();

  public BasicMarker(String name) {
    if (name == null) {
      throw new IllegalArgumentException("A marker name cannot be null");
    }
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void add(Marker reference) {
    if (reference == null) {
      throw new IllegalArgumentException("A null value cannot be added to a Marker as reference.");
    }
    if (this.contains(reference) || reference.contains(this)) {
      //
    } else {
      referenceList.add(reference);
    }
  }

  @Override
  public boolean remove(Marker reference) {
    return referenceList.remove(reference);
  }

  @Override
  public boolean hasReferences() {
    return (referenceList.size() > 0);
  }

  @Override
  public Iterator<Marker> iterator() {
    return referenceList.listIterator();
  }

  @Override
  public boolean contains(Marker other) {
    if (other == null) {
      throw new IllegalArgumentException("Other cannot be null");
    }
    if (this.equals(other)) {
      return true;
    }
    if (hasReferences()) {
      for (Marker ref : referenceList) {
        if (ref.contains(other)) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public boolean contains(String name) {
    if (name == null) {
      throw new IllegalArgumentException("Other cannot be null");
    }
    if (this.name.equals(name)) {
      return true;
    }
    if (hasReferences()) {
      for (Marker ref : referenceList) {
        if (ref.contains(name)) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null) {
      return false;
    }
    if (!(o instanceof Marker)) {
      return false;
    }

    BasicMarker that = (BasicMarker) o;
    return Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public String toString() {
    if (!this.hasReferences()) {
      return this.getName();
    }
    Iterator<Marker> it = this.iterator();
    Marker reference;
    StringBuilder sb = new StringBuilder(this.getName());
    sb.append(' ').append("[ ");
    while (it.hasNext()) {
      reference = it.next();
      sb.append(reference.getName());
      if (it.hasNext()) {
        sb.append(", ");
      }
    }
    sb.append(" ]");
    return sb.toString();
  }
}
