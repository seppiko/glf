/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.event;

import java.util.ArrayList;
import java.util.function.Supplier;
import org.seppiko.glf.api.Level;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.Marker;

/**
 * @author Leonard Woo
 */
public class DefaultLoggerEventBuilder implements LoggerEventBuilder {

  private final Logger logger;
  private final Level level;
  private Marker marker;
  private String message;
  private Throwable cause;
  private final ArrayList<Object> params = new ArrayList<>();

  public DefaultLoggerEventBuilder(Logger logger, Level level) {
    this.logger = logger;
    this.level = level;
  }

  /** {@inheritDoc} */
  @Override
  public LoggerEventBuilder marker(Marker marker) {
    this.marker = marker;
    return this;
  }

  /** {@inheritDoc} */
  @Override
  public LoggerEventBuilder message(String message) {
    this.message = message;
    return this;
  }

  /** {@inheritDoc} */
  @Override
  public LoggerEventBuilder withCause(Throwable cause) {
    this.cause = cause;
    return this;
  }

  /** {@inheritDoc} */
  @Override
  public LoggerEventBuilder addParam(Supplier<?> paramSupplier) {
    params.add(paramSupplier.get());
    return this;
  }

  /** {@inheritDoc} */
  @Override
  public LoggerEventBuilder addParam(Object paramObject) {
    params.add(paramObject);
    return this;
  }

  /** {@inheritDoc} */
  @Override
  public void log() {
    Object[] objects = params.toArray(Object[]::new);
    log(level, marker, message, objects, cause);
  }

  private void log(Level level, Marker marker, String message, Object[] objects, Throwable cause) {
    switch (level) {
      case TRACE -> logger.trace(marker, message, objects, cause);
      case DEBUG -> logger.debug(marker, message, objects, cause);
      case INFO -> logger.info(marker, message, objects, cause);
      case WARN -> logger.warn(marker, message, objects, cause);
      case ERROR -> logger.error(marker, message, objects, cause);
      case FATAL -> logger.fatal(marker, message, objects, cause);
    }
  }

}
