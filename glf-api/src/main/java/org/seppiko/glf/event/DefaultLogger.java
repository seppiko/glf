/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.event;

import org.seppiko.glf.api.Level;
import org.seppiko.glf.api.LocationLogger;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.Marker;
import org.seppiko.glf.event.nop.NOPLogger;

/**
 * A Logger implementation class for registration caller.
 *
 * @author Leonard Woo
 */
public class DefaultLogger implements Logger {

  DefaultLogger(String name) {
    this.name = name;
  }

  private String name;

  @Override
  public String getName() {
    return name;
  }

  private Logger delegate;

  public void setDelegate(Logger delegate) {
    this.delegate = delegate;
  }

  public Logger delegate() {
    if (delegate != null) {
      return delegate;
    }
    return NOPLogger.NOP_LOGGER;
  }

  @Override
  public LoggerEventBuilder atLevel(Level level) {
    return delegate().atLevel(level);
  }

  @Override
  public boolean isEnable(Level level) {
    return delegate().isEnable(level);
  }

  @Override
  public boolean isEnable(Level level, Marker marker) {
    return delegate().isEnable(level, marker);
  }

  @Override
  public boolean isTraceEnabled() {
    return delegate().isTraceEnabled();
  }

  @Override
  public boolean isTraceEnabled(Marker marker) {
    return delegate().isTraceEnabled(marker);
  }

  @Override
  public void trace(String message) {
    delegate().trace(message);
  }

  @Override
  public void trace(String message, Object param) {
    delegate().trace(message, param);
  }

  @Override
  public void trace(String message, Object... params) {
    delegate().trace(message, params);
  }

  @Override
  public void trace(String message, Throwable cause) {
    delegate().trace(message, cause);
  }

  @Override
  public void trace(String message, Object param, Throwable cause) {
    delegate().trace(message, param, cause);
  }

  @Override
  public void trace(String message, Object[] params, Throwable cause) {
    delegate().trace(message, params, cause);
  }

  @Override
  public void trace(Marker marker, String message) {
    delegate().trace(marker, message);
  }

  @Override
  public void trace(Marker marker, String message, Object param) {
    delegate().trace(marker, message, param);
  }

  @Override
  public void trace(Marker marker, String message, Object... params) {
    delegate().trace(marker, message, params);
  }

  @Override
  public void trace(Marker marker, String message, Throwable cause) {
    delegate().trace(marker, message, cause);
  }

  @Override
  public void trace(Marker marker, String message, Object param, Throwable cause) {
    delegate().trace(marker, message, param, cause);
  }

  @Override
  public void trace(Marker marker, String message, Object[] params, Throwable cause) {
    delegate().trace(marker, message, params, cause);
  }

  @Override
  public LoggerEventBuilder atTrace() {
    return delegate().atTrace();
  }

  @Override
  public boolean isDebugEnabled() {
    return delegate().isDebugEnabled();
  }

  @Override
  public boolean isDebugEnabled(Marker marker) {
    return delegate().isDebugEnabled(marker);
  }

  @Override
  public void debug(String message) {
    delegate().debug(message);
  }

  @Override
  public void debug(String message, Object param) {
    delegate().debug(message, param);
  }

  @Override
  public void debug(String message, Object... params) {
    delegate().debug(message, params);
  }

  @Override
  public void debug(String message, Throwable cause) {
    delegate().debug(message, cause);
  }

  @Override
  public void debug(String message, Object param, Throwable cause) {
    delegate().debug(message, new Object[]{ param }, cause);
  }

  @Override
  public void debug(String message, Object[] params, Throwable cause) {
    delegate().debug(message, params, cause);
  }

  @Override
  public void debug(Marker marker, String message) {
    delegate().debug(marker, message);
  }

  @Override
  public void debug(Marker marker, String message, Object param) {
    delegate().debug(marker, message, param);
  }

  @Override
  public void debug(Marker marker, String message, Object... params) {
    delegate().debug(marker, message, params);
  }

  @Override
  public void debug(Marker marker, String message, Throwable cause) {
    delegate().debug(marker, message, cause);
  }

  @Override
  public void debug(Marker marker, String message, Object param, Throwable cause) {
    delegate().debug(marker, message, param, cause);
  }

  @Override
  public void debug(Marker marker, String message, Object[] params, Throwable cause) {
    delegate().debug(marker, message, params, cause);
  }

  @Override
  public LoggerEventBuilder atDebug() {
    return delegate().atDebug();
  }

  @Override
  public boolean isInfoEnabled() {
    return delegate().isInfoEnabled();
  }

  @Override
  public boolean isInfoEnabled(Marker marker) {
    return delegate().isInfoEnabled(marker);
  }

  @Override
  public void info(String message) {
    delegate().info(message);
  }

  @Override
  public void info(String message, Object param) {
    delegate().info(message, param);
  }

  @Override
  public void info(String message, Object... params) {
    delegate().info(message, params);
  }

  @Override
  public void info(String message, Throwable cause) {
    delegate().info(message, cause);
  }

  @Override
  public void info(String message, Object param, Throwable cause) {
    delegate().info(message, param, cause);
  }

  @Override
  public void info(String message, Object[] params, Throwable cause) {
    delegate().info(message, params, cause);
  }

  @Override
  public void info(Marker marker, String message) {
    delegate().info(marker, message);
  }

  @Override
  public void info(Marker marker, String message, Object param) {
    delegate().info(marker, message, param);
  }

  @Override
  public void info(Marker marker, String message, Object... params) {
    delegate().info(marker, message, params);
  }

  @Override
  public void info(Marker marker, String message, Throwable cause) {
    delegate().info(marker, message, cause);
  }

  @Override
  public void info(Marker marker, String message, Object param, Throwable cause) {
    delegate().info(marker, message, param, cause);
  }

  @Override
  public void info(Marker marker, String message, Object[] params, Throwable cause) {
    delegate().info(marker, message, params, cause);
  }

  @Override
  public LoggerEventBuilder atInfo() {
    return delegate().atInfo();
  }

  @Override
  public boolean isWarnEnabled() {
    return delegate().isWarnEnabled();
  }

  @Override
  public boolean isWarnEnabled(Marker marker) {
    return delegate().isWarnEnabled(marker);
  }

  @Override
  public void warn(String message) {
    delegate().warn(message);
  }

  @Override
  public void warn(String message, Object param) {
    delegate().warn(message, param);
  }

  @Override
  public void warn(String message, Object... params) {
    delegate().warn(message, params);
  }

  @Override
  public void warn(String message, Throwable cause) {
    delegate().warn(message, cause);
  }

  @Override
  public void warn(String message, Object param, Throwable cause) {
    delegate().warn(message, param, cause);
  }

  @Override
  public void warn(String message, Object[] params, Throwable cause) {
    delegate().warn(message, params, cause);
  }

  @Override
  public void warn(Marker marker, String message) {
    delegate().warn(marker, message);
  }

  @Override
  public void warn(Marker marker, String message, Object param) {
    delegate().warn(marker, message, param);
  }

  @Override
  public void warn(Marker marker, String message, Object... params) {
    delegate().warn(marker, message, params);
  }

  @Override
  public void warn(Marker marker, String message, Throwable cause) {
    delegate().warn(marker, message, cause);
  }

  @Override
  public void warn(Marker marker, String message, Object param, Throwable cause) {
    delegate().warn(marker, message, param, cause);
  }

  @Override
  public void warn(Marker marker, String message, Object[] params, Throwable cause) {
    delegate().warn(marker, message, params, cause);
  }

  @Override
  public LoggerEventBuilder atWarn() {
    return delegate().atWarn();
  }

  @Override
  public boolean isErrorEnabled() {
    return delegate().isErrorEnabled();
  }

  @Override
  public boolean isErrorEnabled(Marker marker) {
    return delegate().isErrorEnabled(marker);
  }

  @Override
  public void error(String message) {
    delegate().error(message);
  }

  @Override
  public void error(String message, Object param) {
    delegate().error(message, param);
  }

  @Override
  public void error(String message, Object... params) {
    delegate().error(message, params);
  }

  @Override
  public void error(String message, Throwable cause) {
    delegate().error(message, cause);
  }

  @Override
  public void error(String message, Object param, Throwable cause) {
    delegate().error(message, param, cause);
  }

  @Override
  public void error(String message, Object[] params, Throwable cause) {
    delegate().error(message, params, cause);
  }

  @Override
  public void error(Marker marker, String message) {
    delegate().error(marker, message);
  }

  @Override
  public void error(Marker marker, String message, Object param) {
    delegate().error(marker, message, param);
  }

  @Override
  public void error(Marker marker, String message, Object... params) {
    delegate().error(marker, message, params);
  }

  @Override
  public void error(Marker marker, String message, Throwable cause) {
    delegate().error(marker, message, cause);
  }

  @Override
  public void error(Marker marker, String message, Object param, Throwable cause) {
    delegate().error(marker, message, param, cause);
  }

  @Override
  public void error(Marker marker, String message, Object[] params, Throwable cause) {
    delegate().error(marker, message, params, cause);
  }

  @Override
  public LoggerEventBuilder atError() {
    return delegate().atError();
  }

  @Override
  public boolean isFatalEnabled() {
    return delegate().isFatalEnabled();
  }

  @Override
  public boolean isFatalEnabled(Marker marker) {
    return delegate().isFatalEnabled(marker);
  }

  @Override
  public void fatal(String message) {
    delegate().fatal(message);
  }

  @Override
  public void fatal(String message, Object param) {
    delegate().fatal(message, param);
  }

  @Override
  public void fatal(String message, Object... params) {
    delegate().fatal(message, params);
  }

  @Override
  public void fatal(String message, Throwable cause) {
    delegate().fatal(message, cause);
  }

  @Override
  public void fatal(String message, Object param, Throwable cause) {
    delegate().fatal(message, param, cause);
  }

  @Override
  public void fatal(String message, Object[] params, Throwable cause) {
    delegate().fatal(message, params, cause);
  }

  @Override
  public void fatal(Marker marker, String message) {
    delegate().fatal(marker, message);
  }

  @Override
  public void fatal(Marker marker, String message, Object param) {
    delegate().fatal(marker, message, param);
  }

  @Override
  public void fatal(Marker marker, String message, Object... params) {
    delegate().fatal(marker, message, params);
  }

  @Override
  public void fatal(Marker marker, String message, Throwable cause) {
    delegate().fatal(marker, message, cause);
  }

  @Override
  public void fatal(Marker marker, String message, Object param, Throwable cause) {
    delegate().fatal(marker, message, param, cause);
  }

  @Override
  public void fatal(Marker marker, String message, Object[] params, Throwable cause) {
    delegate().fatal(marker, message, params, cause);
  }

  @Override
  public LoggerEventBuilder atFatal() {
    return delegate().atFatal();
  }

}
