/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.event.nop;

import org.seppiko.glf.api.Level;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.Marker;
import org.seppiko.glf.event.LoggerEventBuilder;

/**
 * A direct NOP (no operation) implementation of {@link Logger}.
 *
 * @author Leonard Woo
 */
public class NOPLogger implements Logger {

  public static final NOPLogger NOP_LOGGER = new NOPLogger();

  protected NOPLogger() {}

  @Override
  public String getName() {
    return "NOP";
  }

  @Override
  public LoggerEventBuilder atLevel(Level level) {
    return NOPLoggerEventBuilder.singleton();
  }

  @Override
  public boolean isEnable(Level level) {
    return false;
  }

  @Override
  public boolean isEnable(Level level, Marker marker) {
    return false;
  }

  @Override
  public boolean isTraceEnabled() {
    return false;
  }

  @Override
  public boolean isTraceEnabled(Marker marker) {
    return false;
  }

  @Override
  public void trace(String message) {}

  @Override
  public void trace(String message, Object param) {}

  @Override
  public void trace(String message, Object... params) {}

  @Override
  public void trace(String message, Throwable cause) {}

  @Override
  public void trace(String message, Object param, Throwable cause) {}

  @Override
  public void trace(String message, Object[] params, Throwable cause) {}

  @Override
  public void trace(Marker marker, String message) {}

  @Override
  public void trace(Marker marker, String message, Object param) {}

  @Override
  public void trace(Marker marker, String message, Object... params) {}

  @Override
  public void trace(Marker marker, String message, Throwable cause) {}

  @Override
  public void trace(Marker marker, String message, Object param, Throwable cause) {}

  @Override
  public void trace(Marker marker, String message, Object[] params, Throwable cause) {}

  @Override
  public LoggerEventBuilder atTrace() {
    return NOPLoggerEventBuilder.singleton();
  }

  @Override
  public boolean isDebugEnabled() {
    return false;
  }

  @Override
  public boolean isDebugEnabled(Marker marker) {
    return false;
  }

  @Override
  public void debug(String message) {}

  @Override
  public void debug(String message, Object param) {}

  @Override
  public void debug(String message, Object... params) {}

  @Override
  public void debug(String message, Throwable cause) {}

  @Override
  public void debug(String message, Object param, Throwable cause) {}

  @Override
  public void debug(String message, Object[] params, Throwable cause) {}

  @Override
  public void debug(Marker marker, String message) {}

  @Override
  public void debug(Marker marker, String message, Object param) {}

  @Override
  public void debug(Marker marker, String message, Object... params) {}

  @Override
  public void debug(Marker marker, String message, Throwable cause) {}

  @Override
  public void debug(Marker marker, String message, Object param, Throwable cause) {}

  @Override
  public void debug(Marker marker, String message, Object[] params, Throwable cause) {}

  @Override
  public LoggerEventBuilder atDebug() {
    return NOPLoggerEventBuilder.singleton();
  }

  @Override
  public boolean isInfoEnabled() {
    return false;
  }

  @Override
  public boolean isInfoEnabled(Marker marker) {
    return false;
  }

  @Override
  public void info(String message) {}

  @Override
  public void info(String message, Object param) {}

  @Override
  public void info(String message, Object... params) {}

  @Override
  public void info(String message, Throwable cause) {}

  @Override
  public void info(String message, Object param, Throwable cause) {}

  @Override
  public void info(String message, Object[] params, Throwable cause) {}

  @Override
  public void info(Marker marker, String message) {}

  @Override
  public void info(Marker marker, String message, Object param) {}

  @Override
  public void info(Marker marker, String message, Object... params) {}

  @Override
  public void info(Marker marker, String message, Throwable cause) {}

  @Override
  public void info(Marker marker, String message, Object param, Throwable cause) {}

  @Override
  public void info(Marker marker, String message, Object[] params, Throwable cause) {}

  @Override
  public LoggerEventBuilder atInfo() {
    return NOPLoggerEventBuilder.singleton();
  }

  @Override
  public boolean isWarnEnabled() {
    return false;
  }

  @Override
  public boolean isWarnEnabled(Marker marker) {
    return false;
  }

  @Override
  public void warn(String message) {}

  @Override
  public void warn(String message, Object param) {}

  @Override
  public void warn(String message, Object... params) {}

  @Override
  public void warn(String message, Throwable cause) {}

  @Override
  public void warn(String message, Object param, Throwable cause) {}

  @Override
  public void warn(String message, Object[] params, Throwable cause) {}

  @Override
  public void warn(Marker marker, String message) {}

  @Override
  public void warn(Marker marker, String message, Object param) {}

  @Override
  public void warn(Marker marker, String message, Object... params) {}

  @Override
  public void warn(Marker marker, String message, Throwable cause) {}

  @Override
  public void warn(Marker marker, String message, Object param, Throwable cause) {}

  @Override
  public void warn(Marker marker, String message, Object[] params, Throwable cause) {}

  @Override
  public LoggerEventBuilder atWarn() {
    return NOPLoggerEventBuilder.singleton();
  }

  @Override
  public boolean isErrorEnabled() {
    return false;
  }

  @Override
  public boolean isErrorEnabled(Marker marker) {
    return false;
  }

  @Override
  public void error(String message) {}

  @Override
  public void error(String message, Object param) {}

  @Override
  public void error(String message, Object... params) {}

  @Override
  public void error(String message, Throwable cause) {}

  @Override
  public void error(String message, Object param, Throwable cause) {}

  @Override
  public void error(String message, Object[] params, Throwable cause) {}

  @Override
  public void error(Marker marker, String message) {}

  @Override
  public void error(Marker marker, String message, Object param) {}

  @Override
  public void error(Marker marker, String message, Object... params) {}

  @Override
  public void error(Marker marker, String message, Throwable cause) {}

  @Override
  public void error(Marker marker, String message, Object param, Throwable cause) {}

  @Override
  public void error(Marker marker, String message, Object[] params, Throwable cause) {}

  @Override
  public LoggerEventBuilder atError() {
    return NOPLoggerEventBuilder.singleton();
  }

  @Override
  public boolean isFatalEnabled() {
    return false;
  }

  @Override
  public boolean isFatalEnabled(Marker marker) {
    return false;
  }

  @Override
  public void fatal(String message) {}

  @Override
  public void fatal(String message, Object param) {}

  @Override
  public void fatal(String message, Object... params) {}

  @Override
  public void fatal(String message, Throwable cause) {}

  @Override
  public void fatal(String message, Object param, Throwable cause) {}

  @Override
  public void fatal(String message, Object[] params, Throwable cause) {}

  @Override
  public void fatal(Marker marker, String message) {}

  @Override
  public void fatal(Marker marker, String message, Object param) {}

  @Override
  public void fatal(Marker marker, String message, Object... params) {}

  @Override
  public void fatal(Marker marker, String message, Throwable cause) {}

  @Override
  public void fatal(Marker marker, String message, Object param, Throwable cause) {}

  @Override
  public void fatal(Marker marker, String message, Object[] params, Throwable cause) {}

  @Override
  public LoggerEventBuilder atFatal() {
    return NOPLoggerEventBuilder.singleton();
  }
}
