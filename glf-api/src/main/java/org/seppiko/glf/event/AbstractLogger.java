/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.event;

import org.seppiko.glf.api.Level;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.Marker;

/**
 * Abstract logger implementation
 *
 * @author Leonard Woo
 */
public abstract class AbstractLogger implements Logger {

  protected String name;

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void trace(String message) {
    if ( isTraceEnabled() ) {
      trace(message, (Object) null);
    }
  }

  @Override
  public void trace(String message, Object param) {
    if ( isTraceEnabled() ) {
      trace(message, param, null);
    }
  }

  @Override
  public void trace(String message, Object... params) {
    if ( isTraceEnabled() ) {
      trace(message, params, null);
    }
  }

  @Override
  public void trace(String message, Throwable cause) {
    if ( isTraceEnabled() ) {
      trace(message, null, cause);
    }
  }

  @Override
  public void trace(String message, Object param, Throwable cause) {
    if ( isTraceEnabled() ) {
      trace(message, new Object[]{ param }, cause);
    }
  }

  @Override
  public void trace(String message, Object[] params, Throwable cause) {
    if ( isTraceEnabled() ) {
      trace(null, message, params, cause);
    }
  }

  @Override
  public void trace(Marker marker, String message) {
    if ( isTraceEnabled(marker) ) {
      trace(marker, message, (Object) null);
    }
  }

  @Override
  public void trace(Marker marker, String message, Object param) {
    if ( isTraceEnabled(marker) ) {
      trace(marker, message, param, null);
    }
  }

  @Override
  public void trace(Marker marker, String message, Object... params) {
    if ( isTraceEnabled(marker) ) {
      trace(marker, message, params, null);
    }
  }

  @Override
  public void trace(Marker marker, String message, Throwable cause) {
    if ( isTraceEnabled(marker) ) {
      trace(marker, message, null, cause);
    }
  }

  @Override
  public void trace(Marker marker, String message, Object param, Throwable cause) {
    if ( isTraceEnabled(marker) ) {
      trace(marker, message, new Object[]{ param }, cause);
    }
  }

  @Override
  public void trace(Marker marker, String message, Object[] params, Throwable cause) {
    if ( isTraceEnabled(marker) ) {
      handleNormalizedLoggingCall(Level.TRACE, marker, message, params, cause);
    }
  }

  @Override
  public void debug(String message) {
    if ( isDebugEnabled() ) {
      debug(message, (Object) null);
    }
  }

  @Override
  public void debug(String message, Object param) {
    if ( isDebugEnabled() ) {
      debug(message, param, null);
    }
  }

  @Override
  public void debug(String message, Object... params) {
    if ( isDebugEnabled() ) {
      debug(message, params, null);
    }
  }

  @Override
  public void debug(String message, Throwable cause) {
    if ( isDebugEnabled() ) {
      debug(message, null, cause);
    }
  }

  @Override
  public void debug(String message, Object param, Throwable cause) {
    if ( isDebugEnabled() ) {
      debug(message, new Object[]{ param }, cause);
    }
  }

  @Override
  public void debug(String message, Object[] params, Throwable cause) {
    if ( isDebugEnabled() ) {
      debug(null, message, params, cause);
    }
  }

  @Override
  public void debug(Marker marker, String message) {
    if ( isDebugEnabled(marker) ) {
      debug(marker, message, (Object) null);
    }
  }

  @Override
  public void debug(Marker marker, String message, Object param) {
    if ( isDebugEnabled(marker) ) {
      debug(marker, message, param, null);
    }
  }

  @Override
  public void debug(Marker marker, String message, Object... params) {
    if ( isDebugEnabled(marker) ) {
      debug(marker, message, params, null);
    }
  }

  @Override
  public void debug(Marker marker, String message, Throwable cause) {
    if ( isDebugEnabled(marker) ) {
      debug(marker, message, null, cause);
    }
  }

  @Override
  public void debug(Marker marker, String message, Object param, Throwable cause) {
    if ( isDebugEnabled(marker) ) {
      debug(marker, message, new Object[]{ param }, cause);
    }
  }

  @Override
  public void debug(Marker marker, String message, Object[] params, Throwable cause) {
    if ( isDebugEnabled(marker) ) {
      handleNormalizedLoggingCall(Level.DEBUG, marker, message, params, cause);
    }
  }

  @Override
  public void info(String message) {
    if ( isInfoEnabled() ) {
      info(message, (Object) null);
    }
  }

  @Override
  public void info(String message, Object param) {
    if ( isInfoEnabled() ) {
      info(message, param, null);
    }
  }

  @Override
  public void info(String message, Object... params) {
    if ( isInfoEnabled() ) {
      info(message, params, null);
    }
  }

  @Override
  public void info(String message, Throwable cause) {
    if ( isInfoEnabled() ) {
      info(message, null, cause);
    }
  }

  @Override
  public void info(String message, Object param, Throwable cause) {
    if ( isInfoEnabled() ) {
      info(message, new Object[]{ param }, cause);
    }
  }

  @Override
  public void info(String message, Object[] params, Throwable cause) {
    if ( isInfoEnabled() ) {
      info(null, message, params, cause);
    }
  }

  @Override
  public void info(Marker marker, String message) {
    if ( isInfoEnabled(marker) ) {
      info(marker, message, (Object) null);
    }
  }

  @Override
  public void info(Marker marker, String message, Object param) {
    if ( isInfoEnabled(marker) ) {
      info(marker, message, param, null);
    }
  }

  @Override
  public void info(Marker marker, String message, Object... params) {
    if ( isInfoEnabled(marker) ) {
      info(marker, message, params, null);
    }
  }

  @Override
  public void info(Marker marker, String message, Throwable cause) {
    if ( isInfoEnabled(marker) ) {
      info(marker, message, null, cause);
    }
  }

  @Override
  public void info(Marker marker, String message, Object param, Throwable cause) {
    if ( isInfoEnabled(marker) ) {
      info(marker, message, new Object[]{ param }, cause);
    }
  }

  @Override
  public void info(Marker marker, String message, Object[] params, Throwable cause) {
    if ( isInfoEnabled(marker) ) {
      handleNormalizedLoggingCall(Level.INFO, marker, message, params, cause);
    }
  }

  @Override
  public void warn(String message) {
    if ( isWarnEnabled() ) {
      warn(message, (Object) null);
    }
  }

  @Override
  public void warn(String message, Object param) {
    if ( isWarnEnabled() ) {
      warn(message, param, null);
    }
  }

  @Override
  public void warn(String message, Object... params) {
    if ( isWarnEnabled() ) {
      warn(message, params, null);
    }
  }

  @Override
  public void warn(String message, Throwable cause) {
    if ( isWarnEnabled() ) {
      warn(message, null, cause);
    }
  }

  @Override
  public void warn(String message, Object param, Throwable cause) {
    if ( isWarnEnabled() ) {
      warn(message, new Object[]{ param }, cause);
    }
  }

  @Override
  public void warn(String message, Object[] params, Throwable cause) {
    if ( isWarnEnabled() ) {
      warn(null, message, params, cause);
    }
  }

  @Override
  public void warn(Marker marker, String message) {
    if ( isWarnEnabled(marker) ) {
      warn(marker, message, (Object) null);
    }
  }

  @Override
  public void warn(Marker marker, String message, Object param) {
    if ( isWarnEnabled(marker) ) {
      warn(marker, message, param, null);
    }
  }

  @Override
  public void warn(Marker marker, String message, Object... params) {
    if ( isWarnEnabled(marker) ) {
      warn(marker, message, params, null);
    }
  }

  @Override
  public void warn(Marker marker, String message, Throwable cause) {
    if ( isWarnEnabled(marker) ) {
      warn(marker, message, null, cause);
    }
  }

  @Override
  public void warn(Marker marker, String message, Object param, Throwable cause) {
    if ( isWarnEnabled(marker) ) {
      warn(marker, message, new Object[]{ param }, cause);
    }
  }

  @Override
  public void warn(Marker marker, String message, Object[] params, Throwable cause) {
    if ( isWarnEnabled(marker) ) {
      handleNormalizedLoggingCall(Level.WARN, marker, message, params, cause);
    }
  }

  @Override
  public void error(String message) {
    if ( isErrorEnabled() ) {
      error(message, (Object) null);
    }
  }

  @Override
  public void error(String message, Object param) {
    if ( isErrorEnabled() ) {
      error(message, param, null);
    }
  }

  @Override
  public void error(String message, Object... params) {
    if ( isErrorEnabled() ) {
      error(message, params, null);
    }
  }

  @Override
  public void error(String message, Throwable cause) {
    if ( isErrorEnabled() ) {
      error(message, null, cause);
    }
  }

  @Override
  public void error(String message, Object param, Throwable cause) {
    if ( isErrorEnabled() ) {
      error(message, new Object[]{ param }, cause);
    }
  }

  @Override
  public void error(String message, Object[] params, Throwable cause) {
    if ( isErrorEnabled() ) {
      error(null, message, params, cause);
    }
  }

  @Override
  public void error(Marker marker, String message) {
    if ( isErrorEnabled(marker) ) {
      error(marker, message, (Object) null);
    }
  }

  @Override
  public void error(Marker marker, String message, Object param) {
    if ( isErrorEnabled(marker) ) {
      error(marker, message, param, null);
    }
  }

  @Override
  public void error(Marker marker, String message, Object... params) {
    if ( isErrorEnabled(marker) ) {
      error(marker, message, params, null);
    }
  }

  @Override
  public void error(Marker marker, String message, Throwable cause) {
    if ( isErrorEnabled(marker) ) {
      error(marker, message, null, cause);
    }
  }

  @Override
  public void error(Marker marker, String message, Object param, Throwable cause) {
    if ( isErrorEnabled(marker) ) {
      error(marker, message, new Object[]{ param }, cause);
    }
  }

  @Override
  public void error(Marker marker, String message, Object[] params, Throwable cause) {
    if ( isErrorEnabled(marker) ) {
      handleNormalizedLoggingCall(Level.ERROR, marker, message, params, cause);
    }
  }

  @Override
  public void fatal(String message) {
    if ( isFatalEnabled() ) {
      fatal(message, (Object) null);
    }
  }

  @Override
  public void fatal(String message, Object param) {
    if ( isFatalEnabled() ) {
      fatal(message, param, null);
    }
  }

  @Override
  public void fatal(String message, Object... params) {
    if ( isFatalEnabled() ) {
      fatal(message, params, null);
    }
  }

  @Override
  public void fatal(String message, Throwable cause) {
    if ( isFatalEnabled() ) {
      fatal(message, null, cause);
    }
  }

  @Override
  public void fatal(String message, Object param, Throwable cause) {
    if ( isFatalEnabled() ) {
      fatal(message, new Object[]{ param }, cause);
    }
  }

  @Override
  public void fatal(String message, Object[] params, Throwable cause) {
    if ( isFatalEnabled() ) {
      fatal(null, message, params, cause);
    }
  }

  @Override
  public void fatal(Marker marker, String message) {
    if ( isFatalEnabled(marker) ) {
      fatal(marker, message, (Object) null);
    }
  }

  @Override
  public void fatal(Marker marker, String message, Object param) {
    if ( isFatalEnabled(marker) ) {
      fatal(marker, message, param, null);
    }
  }

  @Override
  public void fatal(Marker marker, String message, Object... params) {
    if ( isFatalEnabled(marker) ) {
      fatal(marker, message, params, null);
    }
  }

  @Override
  public void fatal(Marker marker, String message, Throwable cause) {
    if ( isFatalEnabled(marker) ) {
      fatal(marker, message, null, cause);
    }
  }

  @Override
  public void fatal(Marker marker, String message, Object param, Throwable cause) {
    if ( isFatalEnabled(marker) ) {
      fatal(marker, message, new Object[]{ param }, cause);
    }
  }

  @Override
  public void fatal(Marker marker, String message, Object[] params, Throwable cause) {
    if ( isFatalEnabled(marker) ) {
      handleNormalizedLoggingCall(Level.TRACE, marker, message, params, cause);
    }
  }

  /**
   * FQCN is a real service name.
   *
   * @return Fully Qualified Caller Name
   */
  protected abstract String getFullyQualifiedCallerName();

  /**
   * A method like {@link org.seppiko.glf.api.LocationLogger} for log caller
   *
   * @param level Logger level
   * @param marker Logger marker
   * @param msg Logger message
   * @param arguments Logger argument object
   * @param cause Logger Throwable
   */
  protected abstract void handleNormalizedLoggingCall(Level level, Marker marker, String msg,
      Object[] arguments, Throwable cause);
}
