/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.event;

import java.util.function.Supplier;
import org.seppiko.glf.api.Marker;

/**
 * Logger event builder
 *
 * @author Leonard Woo
 */
public interface LoggerEventBuilder {

  /**
   * A {@link Marker marker} to the event being built.
   *
   * @param marker a Marker instance.
   * @return a LoggerEventBuilder, usually <b>this</b>.
   */
  LoggerEventBuilder marker(Marker marker);

  /**
   * Set message
   *
   * @param message the message object to log.
   * @return a LoggerEventBuilder, usually <b>this</b>.
   */
  LoggerEventBuilder message(String message);

  /**
   * Set throwable
   *
   * @param cause the Throwable to log, including its stack trace.
   * @return a LoggerEventBuilder, usually <b>this</b>.
   */
  LoggerEventBuilder withCause(Throwable cause);

  /**
   * Set object parameter
   *
   * @param paramSupplier the parameters to log.
   * @return a LoggerEventBuilder, usually <b>this</b>.
   */
  LoggerEventBuilder addParam(Supplier<?> paramSupplier);

  /**
   * Set object parameter
   *
   * @param paramObject the parameters to log.
   * @return a LoggerEventBuilder, usually <b>this</b>.
   */
  LoggerEventBuilder addParam(Object paramObject);

  /** Log builder. */
  void log();
}
