/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.event.nop;

import java.util.function.Supplier;
import org.seppiko.glf.api.Marker;
import org.seppiko.glf.event.LoggerEventBuilder;

/**
 * @author Leonard Woo
 */
public class NOPLoggerEventBuilder implements LoggerEventBuilder {

  private static final NOPLoggerEventBuilder SINGLETON = new NOPLoggerEventBuilder();

  private NOPLoggerEventBuilder() {
  }

  public static LoggerEventBuilder singleton() {
    return SINGLETON;
  }

  @Override
  public LoggerEventBuilder marker(Marker marker) {
    return this;
  }

  @Override
  public LoggerEventBuilder message(String message) {
    return this;
  }

  @Override
  public LoggerEventBuilder withCause(Throwable cause) {
    return this;
  }

  @Override
  public LoggerEventBuilder addParam(Supplier<?> paramSupplier) {
    return this;
  }

  @Override
  public LoggerEventBuilder addParam(Object paramObject) {
    return this;
  }

  @Override
  public void log() {
  }
}
