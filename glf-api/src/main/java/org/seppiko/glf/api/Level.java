/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.api;

import static org.seppiko.glf.api.EventConstants.DEBUG_INT;
import static org.seppiko.glf.api.EventConstants.ERROR_INT;
import static org.seppiko.glf.api.EventConstants.FATAL_INT;
import static org.seppiko.glf.api.EventConstants.INFO_INT;
import static org.seppiko.glf.api.EventConstants.TRACE_INT;
import static org.seppiko.glf.api.EventConstants.WARN_INT;

/**
 * Logger Level
 *
 * <ul>
 *   <li>{@link #TRACE} a lot of data
 *   <li>{@link #DEBUG}
 *   <li>{@link #INFO}
 *   <li>{@link #WARN}
 *   <li>{@link #ERROR}
 *   <li>{@link #FATAL} little data
 * </ul>
 *
 * @author Leonard Woo
 */
public enum Level {

  /** Level {@link #TRACE} */
  TRACE(TRACE_INT),

  /** Level {@link #DEBUG} */
  DEBUG(DEBUG_INT),

  /** Level {@link #INFO} */
  INFO(INFO_INT),

  /** Level {@link #WARN} */
  WARN(WARN_INT),

  /** Level {@link #ERROR} */
  ERROR(ERROR_INT),

  /** Level {@link #FATAL} */
  FATAL(FATAL_INT);

  final int value;

  Level(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  public static Level getLevel(int level) {
    return switch (level) {
      case TRACE_INT -> Level.TRACE;
      case DEBUG_INT -> Level.DEBUG;
      case INFO_INT -> Level.INFO;
      case WARN_INT -> Level.WARN;
      case ERROR_INT -> Level.ERROR;
      case FATAL_INT -> Level.FATAL;
      default ->
          throw new IllegalArgumentException("Level integer [" + level + "] not recognized.");
    };
  }

}
