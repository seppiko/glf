/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.api;

import org.seppiko.glf.spi.GLFServiceProvider;
import org.seppiko.glf.utils.GLFReport;

/**
 * MarkerFactory is a utility class producing {@link Marker} instances as appropriate for the
 * logging system currently in use.
 *
 * @author Leonard Woo
 */
public class MarkerFactory {

  private static IMarkerFactory MARKER_FACTORY;

  static {
    GLFServiceProvider serviceProvider = LoggerFactory.getServiceProvider();
    if (serviceProvider != null) {
      MARKER_FACTORY = serviceProvider.getMarkerFactory();
    } else {
      GLFReport.err("Failed to find provider");
    }
  }

  /**
   * Return a Marker instance as specified by the name parameter using the previously bound
   * {@link IMarkerFactory} instance.
   *
   * @param name The name of the {@link Marker} object to return.
   * @return a marker.
   */
  public static Marker getMarker(String name) {
    return MARKER_FACTORY.getMarker(name);
  }

  /**
   * Create a marker which is detached (even at birth) from the MarkerFactory.
   *
   * @param name the name of the marker.
   * @return a dangling marker.
   */
  public static Marker getDetachedMarker(String name) {
    return MARKER_FACTORY.getDetachedMarker(name);
  }

  /**
   * Return the {@link IMarkerFactory} instance in use.
   *
   * @return the IMarkerFactory instance in use.
   */
  public static IMarkerFactory getIMarkerFactory() {
    return MARKER_FACTORY;
  }
}
