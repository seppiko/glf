/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.api;

import java.util.Arrays;
import java.util.function.Supplier;

/**
 * Logger sub-interface for extended service with FQCN (Fully Qualified Caller Name)
 *
 * @author Leonard Woo
 */
public interface LocationLogger extends Logger {

  /**
   * Location logger
   *
   * @param fqcn The fully qualified class name of the logger entry point, used to determine the
   *    caller class and method when location information needs to be logged.
   * @param level The logger Level.
   * @param marker A Marker or null.
   * @param message The message format.
   * @param params the message parameters.
   * @param cause the exception to log, including its stack trace.
   */
  void log(String fqcn, Level level, Marker marker, String message, Object[] params, Throwable cause);

  /**
   * Location logger
   *
   * @param fqcn The fully qualified class name of the logger entry point, used to determine the
   *    caller class and method when location information needs to be logged.
   * @param level The logger Level.
   * @param marker A Marker or null.
   * @param message The message format.
   * @param paramSuppliers An array of functions, which when called, produce the desired log
   *    message parameters.
   * @param cause the exception to log, including its stack trace.
   */
  void log(String fqcn, Level level, Marker marker, String message, Supplier<?>[] paramSuppliers, Throwable cause);
}
