/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.api;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;
import org.seppiko.glf.event.DefaultLogger;
import org.seppiko.glf.event.DefaultServiceProvider;
import org.seppiko.glf.event.nop.NOPLogger;
import org.seppiko.glf.event.nop.NOPServiceProvider;
import org.seppiko.glf.spi.GLFServiceProvider;
import org.seppiko.glf.utils.GLFReport;

/**
 * The <code>LoggerFactory</code> is a utility class producing Loggers for various logging APIs.
 * Other implementations such as {@link NOPLogger NOPLogger} and SimpleLogger are also supported.
 *
 * @author Leonard Woo
 */
public final class LoggerFactory {

  static volatile GLFServiceProvider PROVIDER;

  // Logger factory status codes
  private enum status {
    UNINITIALIZED(0),
    ONGOING_INITIALIZATION(1),
    FAILED_INITIALIZATION(2),
    SUCCESSFUL_INITIALIZATION(3),
    NOP_PROVIDER_INITIALIZATION(4);

    final int code;
    status(int code) {
      this.code = code;
    }
  }

  // Logger factory status
  private static volatile status STATUS = status.UNINITIALIZED;

  private static final DefaultServiceProvider DEFAULT_PROVIDER = new DefaultServiceProvider();
  private static final NOPServiceProvider NOP_PROVIDER = new NOPServiceProvider();

  private LoggerFactory() {
  }

  /**
   * Return a logger named according to the name parameter using the
   * statically bound {@link ILoggerFactory} instance.
   *
   * @param name The name of the logger.
   * @return the Logger instance.
   */
  public static Logger getLogger(String name) {
    return getLoggerFactory().getLogger(name);
  }

  /**
   * Return a logger named corresponding to the class passed as parameter,
   * using the statically bound {@link ILoggerFactory} instance.
   *
   * @param clazz the returned logger will be named after clazz.
   * @return the Logger instance.
   */
  public static Logger getLogger(Class<?> clazz) {
    return getLogger(clazz.getName());
  }

  /**
   * Return the {@link ILoggerFactory} instance in use.
   *
   * <p> ILoggerFactory instance is bound with this class at compile time.
   *
   * @return the ILoggerFactory instance in use.
   */
  public static ILoggerFactory getLoggerFactory() {
    return getServiceProvider().getILoggerFactory();
  }

  static GLFServiceProvider getServiceProvider() {
    if (STATUS == status.UNINITIALIZED) {
      synchronized (LoggerFactory.class) {
        if (STATUS == status.UNINITIALIZED) {
          STATUS = status.ONGOING_INITIALIZATION;
          bind();
        }
      }
    }

    return switch (STATUS) {
      case SUCCESSFUL_INITIALIZATION -> PROVIDER;
      case NOP_PROVIDER_INITIALIZATION -> NOP_PROVIDER;
      case FAILED_INITIALIZATION -> throw new IllegalStateException(
          "LoggerFactory in failed state. Original exception was thrown EARLIER.");
      case ONGOING_INITIALIZATION -> DEFAULT_PROVIDER;
      default -> throw new IllegalStateException("Unreachable code");
    };
  }

  private static void bind() {
    try {
      List<GLFServiceProvider> providers = findGLFServiceProvider();
      if (providers.size() > 1) {
        GLFReport.err("Class path contains multiple GLF providers.");
        for (GLFServiceProvider provider : providers) {
          GLFReport.err("Found provider [" + provider + "]");
        }
      }
      if (!providers.isEmpty()) {
        PROVIDER = providers.get(0);
        PROVIDER.initialize();
        STATUS = status.SUCCESSFUL_INITIALIZATION;

        if (providers.size() > 1) {
          GLFReport.info("Actual provider is of type [" + providers.get(0) + "]");
        }
      } else {
        STATUS = status.NOP_PROVIDER_INITIALIZATION;
        GLFReport.err("No GLF service providers were found.");
        GLFReport.err("Defaulting to no-operation (NOP) logger implementation");
      }

      synchronized (DEFAULT_PROVIDER) {
        for (DefaultLogger defaultLogger : DEFAULT_PROVIDER.getLoggerFactory().getLoggers()) {
          Logger logger = getLogger(defaultLogger.getName());
          defaultLogger.setDelegate(logger);
        }
      }

      DEFAULT_PROVIDER.getLoggerFactory().clear();
    } catch (Exception e) {
      STATUS = status.FAILED_INITIALIZATION;
      GLFReport.err("Failed to instantiate GLF LoggerFactory", e);
      throw new IllegalStateException("Unexpected initialization failure", e);
    }
  }

  private static List<GLFServiceProvider> findGLFServiceProvider() {
    ServiceLoader<GLFServiceProvider> loader = ServiceLoader.load(GLFServiceProvider.class);
    List<GLFServiceProvider> providers = new ArrayList<>();
    loader.forEach(providers::add);
    return providers;
  }

}
