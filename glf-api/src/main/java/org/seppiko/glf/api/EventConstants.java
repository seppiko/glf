/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.api;

/**
 * A Event Constants class like environment class
 *
 * @author Leonard Woo
 */
public final class EventConstants {

  protected static final int TRACE_INT = 10;
  protected static final int DEBUG_INT = 20;
  protected static final int INFO_INT  = 30;
  protected static final int WARN_INT  = 40;
  protected static final int ERROR_INT = 50;
  protected static final int FATAL_INT = 60;

}
