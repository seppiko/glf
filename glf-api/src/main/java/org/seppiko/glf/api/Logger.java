/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.api;

import org.seppiko.glf.event.LoggerEventBuilder;
import org.seppiko.glf.event.nop.NOPLoggerEventBuilder;

/**
 * The org.seppiko.glf.api.Logger interface is the main user entry point of GLF API.
 * It is expected that logging takes place through concrete implementations
 * of this interface.
 *
 * @author Leonard Woo
 */
public interface Logger {

  /** Case-insensitive String constant used to retrieve the name of the root logger. */
  String ROOT_LOGGER_NAME = "ROOT";

  /**
   * Return the name of this <code>Logger</code> instance.
   *
   * @return name of this logger instance.
   */
  String getName();

  /**
   * Make a new {@link LoggerEventBuilder} instance as appropriate for this logger and the desired
   * {@link Level} passed as parameter. If this Logger is disabled for the given Level, then a
   * {@link NOPLoggerEventBuilder} is returned.
   *
   * @param level desired level for the event builder.
   * @return a new {@link LoggerEventBuilder} instance as appropriate for this logger.
   */
  LoggerEventBuilder atLevel(Level level);

  /**
   * Returns whether this Logger is enabled for a given {@link Level}.
   *
   * @param level The level to take into consideration.
   * @return true if enabled, false otherwise.
   */
  boolean isEnable(Level level);

  /**
   * Returns whether this Logger is enabled for a given {@link Level} and {@link  Marker}.
   *
   * @param level The level to take into consideration.
   * @param marker The marker data to take into consideration.
   * @return true if enabled, false otherwise.
   */
  boolean isEnable(Level level, Marker marker);

  /**
   * Is the logger instance enabled for the TRACE level?
   *
   * @return True if this Logger is enabled for the TRACE level, false otherwise.
   */
  boolean isTraceEnabled();

  /**
   * Similar to {@link #isTraceEnabled()} method except that the marker data is also taken into
   * account.
   *
   * @param marker The marker data to take into consideration.
   * @return True if this Logger is enabled for the TRACE level, false otherwise.
   */
  boolean isTraceEnabled(Marker marker);

  /**
   * Log a message at the TRACE level according to the specified format and arguments.
   *
   * @param message the message string to be logged.
   */
  void trace(String message);

  /**
   * Log a message at the TRACE level according to the specified format and argument.
   *
   * @param message the message string to be logged.
   * @param param the argument.
   */
  void trace(String message, Object param);

  /**
   * Log a message at the TRACE level according to the specified format and arguments.
   *
   * @param message the message string to be logged.
   * @param params an array of arguments.
   */
  void trace(String message, Object... params);

  /**
   * Log an exception (throwable) at the TRACE level with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param cause the exception (throwable) to log.
   */
  void trace(String message, Throwable cause);

  /**
   * Log a message at the TRACE level according to the specified format and argument and
   * an exception (throwable) with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param param the argument.
   * @param cause the exception (throwable) to log.
   */
  void trace(String message, Object param, Throwable cause);

  /**
   * Log a message at the TRACE level according to the specified format and arguments and
   * an exception (throwable) with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param params an array of arguments.
   * @param cause the exception (throwable) to log.
   */
  void trace(String message, Object[] params, Throwable cause);

  /**
   * Log a message with the specific Marker at the TRACE level.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   */
  void trace(Marker marker, String message);

  /**
   * This method is similar to {@link #trace(String, Object)} method except that the marker data
   * is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param param the argument.
   */
  void trace(Marker marker, String message, Object param);

  /**
   * This method is similar to {@link #trace(String, Object...)} method except that the marker
   * data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param params an array of arguments.
   */
  void trace(Marker marker, String message, Object... params);

  /**
   * This method is similar to {@link #trace(String, Throwable)} method except that the marker
   * data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param cause the exception (throwable) to log.
   */
  void trace(Marker marker, String message, Throwable cause);

  /**
   * This method is similar to {@link #trace(String, Object, Throwable)} method except that the
   * marker data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param param the argument.
   * @param cause the exception (throwable) to log.
   */
  void trace(Marker marker, String message, Object param, Throwable cause);

  /**
   * This method is similar to {@link #trace(String, Object[], Throwable)} method except that the
   * marker data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param params an array of arguments.
   * @param cause the exception (throwable) to log.
   */
  void trace(Marker marker, String message, Object[] params, Throwable cause);

  /**
   * Entry point for fluent-logging for {@link Level#TRACE} level.
   *
   * @return LoggingEventBuilder instance as appropriate for level TRACE
   */
  LoggerEventBuilder atTrace();

  /**
   * Is the logger instance enabled for the DEBUG level?
   *
   * @return True if this Logger is enabled for the DEBUG level, false otherwise.
   */
  boolean isDebugEnabled();

  /**
   * Similar to {@link #isDebugEnabled()} method except that the marker data is also taken into
   * account.
   *
   * @param marker The marker data to take into consideration.
   * @return True if this Logger is enabled for the DEBUG level, false otherwise.
   */
  boolean isDebugEnabled(Marker marker);

  /**
   * Log a message at the DEBUG level according to the specified format and arguments.
   *
   * @param message the message string to be logged.
   */
  void debug(String message);

  /**
   * Log a message at the DEBUG level according to the specified format and argument.
   *
   * @param message the message string to be logged.
   * @param param the argument.
   */
  void debug(String message, Object param);

  /**
   * Log a message at the DEBUG level according to the specified format and arguments.
   *
   * @param message the message string to be logged.
   * @param params an array of arguments.
   */
  void debug(String message, Object... params);

  /**
   * Log an exception (throwable) at the DEBUG level with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param cause the exception (throwable) to log.
   */
  void debug(String message, Throwable cause);

  /**
   * Log a message at the DEBUG level according to the specified format and argument and
   * an exception (throwable) with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param param the argument.
   * @param cause the exception (throwable) to log.
   */
  void debug(String message, Object param, Throwable cause);

  /**
   * Log a message at the DEBUG level according to the specified format and arguments and
   * an exception (throwable) with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param params an array of arguments.
   * @param cause the exception (throwable) to log.
   */
  void debug(String message, Object[] params, Throwable cause);

  /**
   * Log a message with the specific Marker at the DEBUG level.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   */
  void debug(Marker marker, String message);

  /**
   * This method is similar to {@link #debug(String, Object)} method except that the marker data
   * is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param param the argument.
   */
  void debug(Marker marker, String message, Object param);

  /**
   * This method is similar to {@link #debug(String, Object...)} method except that the marker
   * data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param params an array of arguments.
   */
  void debug(Marker marker, String message, Object... params);

  /**
   * This method is similar to {@link #debug(String, Throwable)} method except that the marker
   * data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param cause the exception (throwable) to log.
   */
  void debug(Marker marker, String message, Throwable cause);

  /**
   * This method is similar to {@link #debug(String, Object, Throwable)} method except that the
   * marker data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param param the argument.
   * @param cause the exception (throwable) to log.
   */
  void debug(Marker marker, String message, Object param, Throwable cause);

  /**
   * This method is similar to {@link #debug(String, Object[], Throwable)} method except that the
   * marker data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param params an array of arguments.
   * @param cause the exception (throwable) to log.
   */
  void debug(Marker marker, String message, Object[] params, Throwable cause);

  /**
   * Entry point for fluent-logging for {@link Level#DEBUG} level.
   *
   * @return LoggingEventBuilder instance as appropriate for level DEBUG
   */
  LoggerEventBuilder atDebug();

  /**
   * Is the logger instance enabled for the INFO level?
   *
   * @return True if this Logger is enabled for the INFO level, false otherwise.
   */
  boolean isInfoEnabled();

  /**
   * Similar to {@link #isInfoEnabled()} method except that the marker data is also taken into
   * account.
   *
   * @param marker The marker data to take into consideration.
   * @return True if this Logger is enabled for the INFO level, false otherwise.
   */
  boolean isInfoEnabled(Marker marker);

  /**
   * Log a message at the INFO level according to the specified format and arguments.
   *
   * @param message the message string to be logged.
   */
  void info(String message);

  /**
   * Log a message at the INFO level according to the specified format and argument.
   *
   * @param message the message string to be logged.
   * @param param the argument.
   */
  void info(String message, Object param);

  /**
   * Log a message at the INFO level according to the specified format and arguments.
   *
   * @param message the message string to be logged.
   * @param params an array of arguments.
   */
  void info(String message, Object... params);

  /**
   * Log an exception (throwable) at the INFO level with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param cause the exception (throwable) to log.
   */
  void info(String message, Throwable cause);

  /**
   * Log a message at the INFO level according to the specified format and argument and
   * an exception (throwable) with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param param the argument.
   * @param cause the exception (throwable) to log.
   */
  void info(String message, Object param, Throwable cause);

  /**
   * Log a message at the INFO level according to the specified format and arguments and
   * an exception (throwable) with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param params an array of arguments.
   * @param cause the exception (throwable) to log.
   */
  void info(String message, Object[] params, Throwable cause);

  /**
   * Log a message with the specific Marker at the INFO level.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   */
  void info(Marker marker, String message);

  /**
   * This method is similar to {@link #info(String, Object)} method except that the marker data
   * is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param param the argument.
   */
  void info(Marker marker, String message, Object param);

  /**
   * This method is similar to {@link #info(String, Object...)} method except that the marker
   * data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param params an array of arguments.
   */
  void info(Marker marker, String message, Object... params);

  /**
   * This method is similar to {@link #info(String, Throwable)} method except that the marker
   * data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param cause the exception (throwable) to log.
   */
  void info(Marker marker, String message, Throwable cause);

  /**
   * This method is similar to {@link #info(String, Object, Throwable)} method except that the
   * marker data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param param the argument.
   * @param cause the exception (throwable) to log.
   */
  void info(Marker marker, String message, Object param, Throwable cause);

  /**
   * This method is similar to {@link #info(String, Object[], Throwable)} method except that the
   * marker data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param params an array of arguments.
   * @param cause the exception (throwable) to log.
   */
  void info(Marker marker, String message, Object[] params, Throwable cause);

  /**
   * Entry point for fluent-logging for {@link Level#INFO} level.
   *
   * @return LoggingEventBuilder instance as appropriate for level INFO
   */
  LoggerEventBuilder atInfo();

  /**
   * Is the logger instance enabled for the WARN level?
   *
   * @return True if this Logger is enabled for the WARN level, false otherwise.
   */
  boolean isWarnEnabled();

  /**
   * Similar to {@link #isWarnEnabled()} method except that the marker data is also taken into
   * account.
   *
   * @param marker The marker data to take into consideration.
   * @return True if this Logger is enabled for the WARN level, false otherwise.
   */
  boolean isWarnEnabled(Marker marker);

  /**
   * Log a message at the WARN level according to the specified format and arguments.
   *
   * @param message the message string to be logged.
   */
  void warn(String message);

  /**
   * Log a message at the WARN level according to the specified format and argument.
   *
   * @param message the message string to be logged.
   * @param param the argument.
   */
  void warn(String message, Object param);

  /**
   * Log a message at the WARN level according to the specified format and arguments.
   *
   * @param message the message string to be logged.
   * @param params an array of arguments.
   */
  void warn(String message, Object... params);

  /**
   * Log an exception (throwable) at the WARN level with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param cause the exception (throwable) to log.
   */
  void warn(String message, Throwable cause);

  /**
   * Log a message at the WARN level according to the specified format and argument and
   * an exception (throwable) with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param param the argument.
   * @param cause the exception (throwable) to log.
   */
  void warn(String message, Object param, Throwable cause);

  /**
   * Log a message at the WARN level according to the specified format and arguments and
   * an exception (throwable) with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param params an array of arguments.
   * @param cause the exception (throwable) to log.
   */
  void warn(String message, Object[] params, Throwable cause);

  /**
   * Log a message with the specific Marker at the WARN level.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   */
  void warn(Marker marker, String message);

  /**
   * This method is similar to {@link #warn(String, Object)} method except that the marker data
   * is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param param the argument.
   */
  void warn(Marker marker, String message, Object param);

  /**
   * This method is similar to {@link #warn(String, Object...)} method except that the marker
   * data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param params an array of arguments.
   */
  void warn(Marker marker, String message, Object... params);

  /**
   * This method is similar to {@link #warn(String, Throwable)} method except that the marker
   * data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param cause the exception (throwable) to log.
   */
  void warn(Marker marker, String message, Throwable cause);

  /**
   * This method is similar to {@link #warn(String, Object, Throwable)} method except that the
   * marker data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param param the argument.
   * @param cause the exception (throwable) to log.
   */
  void warn(Marker marker, String message, Object param, Throwable cause);

  /**
   * This method is similar to {@link #warn(String, Object[], Throwable)} method except that the
   * marker data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param params an array of arguments.
   * @param cause the exception (throwable) to log.
   */
  void warn(Marker marker, String message, Object[] params, Throwable cause);

  /**
   * Entry point for fluent-logging for {@link Level#WARN} level.
   *
   * @return LoggingEventBuilder instance as appropriate for level WARN
   */
  LoggerEventBuilder atWarn();

  /**
   * Is the logger instance enabled for the ERROR level?
   *
   * @return True if this Logger is enabled for the ERROR level, false otherwise.
   */
  boolean isErrorEnabled();

  /**
   * Similar to {@link #isErrorEnabled()} method except that the marker data is also taken into
   * account.
   *
   * @param marker The marker data to take into consideration.
   * @return True if this Logger is enabled for the ERROR level, false otherwise.
   */
  boolean isErrorEnabled(Marker marker);

  /**
   * Log a message at the ERROR level according to the specified format and arguments.
   *
   * @param message the message string to be logged.
   */
  void error(String message);

  /**
   * Log a message at the ERROR level according to the specified format and argument.
   *
   * @param message the message string to be logged.
   * @param param the argument.
   */
  void error(String message, Object param);

  /**
   * Log a message at the ERROR level according to the specified format and arguments.
   *
   * @param message the message string to be logged.
   * @param params an array of arguments.
   */
  void error(String message, Object... params);

  /**
   * Log an exception (throwable) at the ERROR level with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param cause the exception (throwable) to log.
   */
  void error(String message, Throwable cause);

  /**
   * Log a message at the ERROR level according to the specified format and argument and
   * an exception (throwable) with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param param the argument.
   * @param cause the exception (throwable) to log.
   */
  void error(String message, Object param, Throwable cause);

  /**
   * Log a message at the ERROR level according to the specified format and arguments and
   * an exception (throwable) with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param params an array of arguments.
   * @param cause the exception (throwable) to log.
   */
  void error(String message, Object[] params, Throwable cause);

  /**
   * Log a message with the specific Marker at the ERROR level.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   */
  void error(Marker marker, String message);

  /**
   * This method is similar to {@link #error(String, Object)} method except that the marker data
   * is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param param the argument.
   */
  void error(Marker marker, String message, Object param);

  /**
   * This method is similar to {@link #error(String, Object...)} method except that the marker
   * data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param params an array of arguments.
   */
  void error(Marker marker, String message, Object... params);

  /**
   * This method is similar to {@link #error(String, Throwable)} method except that the marker
   * data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param cause the exception (throwable) to log.
   */
  void error(Marker marker, String message, Throwable cause);

  /**
   * This method is similar to {@link #error(String, Object, Throwable)} method except that the
   * marker data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param param the argument.
   * @param cause the exception (throwable) to log.
   */
  void error(Marker marker, String message, Object param, Throwable cause);

  /**
   * This method is similar to {@link #error(String, Object[], Throwable)} method except that the
   * marker data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param params an array of arguments.
   * @param cause the exception (throwable) to log.
   */
  void error(Marker marker, String message, Object[] params, Throwable cause);

  /**
   * Entry point for fluent-logging for {@link Level#ERROR} level.
   *
   * @return LoggingEventBuilder instance as appropriate for level ERROR
   */
  LoggerEventBuilder atError();

  /**
   * Is the logger instance enabled for the FATAL level?
   *
   * @return True if this Logger is enabled for the FATAL level, false otherwise.
   */
  boolean isFatalEnabled();

  /**
   * Similar to {@link #isFatalEnabled()} method except that the marker data is also taken into
   * account.
   *
   * @param marker The marker data to take into consideration.
   * @return True if this Logger is enabled for the FATAL level, false otherwise.
   */
  boolean isFatalEnabled(Marker marker);

  /**
   * Log a message at the FATAL level according to the specified format and arguments.
   *
   * @param message the message string to be logged.
   */
  void fatal(String message);

  /**
   * Log a message at the FATAL level according to the specified format and argument.
   *
   * @param message the message string to be logged.
   * @param param the argument.
   */
  void fatal(String message, Object param);

  /**
   * Log a message at the FATAL level according to the specified format and arguments.
   *
   * @param message the message string to be logged.
   * @param params an array of arguments.
   */
  void fatal(String message, Object... params);

  /**
   * Log an exception (throwable) at the FATAL level with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param cause the exception (throwable) to log.
   */
  void fatal(String message, Throwable cause);

  /**
   * Log a message at the FATAL level according to the specified format and argument and
   * an exception (throwable) with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param param the argument.
   * @param cause the exception (throwable) to log.
   */
  void fatal(String message, Object param, Throwable cause);

  /**
   * Log a message at the FATAL level according to the specified format and arguments and
   * an exception (throwable) with an accompanying message.
   *
   * @param message the message string to be logged.
   * @param params an array of arguments.
   * @param cause the exception (throwable) to log.
   */
  void fatal(String message, Object[] params, Throwable cause);

  /**
   * Log a message with the specific Marker at the FATAL level.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   */
  void fatal(Marker marker, String message);

  /**
   * This method is similar to {@link #fatal(String, Object)} method except that the marker data
   * is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param param the argument.
   */
  void fatal(Marker marker, String message, Object param);

  /**
   * This method is similar to {@link #fatal(String, Object...)} method except that the marker
   * data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param params an array of arguments.
   */
  void fatal(Marker marker, String message, Object... params);

  /**
   * This method is similar to {@link #fatal(String, Throwable)} method except that the marker
   * data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param cause the exception (throwable) to log.
   */
  void fatal(Marker marker, String message, Throwable cause);

  /**
   * This method is similar to {@link #fatal(String, Object, Throwable)} method except that the
   * marker data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param param the argument.
   * @param cause the exception (throwable) to log.
   */
  void fatal(Marker marker, String message, Object param, Throwable cause);

  /**
   * This method is similar to {@link #fatal(String, Object[], Throwable)} method except that the
   * marker data is also taken into consideration.
   *
   * @param marker the marker data specific to this log statement.
   * @param message the message string to be logged.
   * @param params an array of arguments.
   * @param cause the exception (throwable) to log.
   */
  void fatal(Marker marker, String message, Object[] params, Throwable cause);

  /**
   * Entry point for fluent-logging for {@link Level#FATAL} level.
   *
   * @return LoggingEventBuilder instance as appropriate for level FATAL
   */
  LoggerEventBuilder atFatal();
}
