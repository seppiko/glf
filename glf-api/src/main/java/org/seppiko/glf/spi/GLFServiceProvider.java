/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.spi;

import org.seppiko.glf.api.ILoggerFactory;
import org.seppiko.glf.api.IMarkerFactory;
import org.seppiko.glf.api.LoggerFactory;
import org.seppiko.glf.api.MarkerFactory;

/**
 * GLF Service Provider interface.
 * This interface based on {@link java.util.ServiceLoader} paradigm.
 *
 * @author Leonard Woo
 */
public interface GLFServiceProvider {

  /**
   * Return the instance of {@link ILoggerFactory} that
   * {@link LoggerFactory} class should bind to.
   *
   * @return instance of {@link ILoggerFactory}
   */
  ILoggerFactory getILoggerFactory();

  /**
   * Return the instance of {@link IMarkerFactory} that
   * {@link MarkerFactory} class should bind to.
   *
   * @return instance of {@link IMarkerFactory}
   */
  IMarkerFactory getMarkerFactory();

  /**
   * Initialize the logging back-end.
   *
   * <p><b>WARNING:</b> This method is intended to be called once by
   * {@link LoggerFactory} class and from nowhere else.
   *
   */
  void initialize();
}
