/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.bridge;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import org.seppiko.glf.api.LocationLogger;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;

/**
 * Bridge/route all JUL log records to the GLF API.
 *
 * <p>Essentially, the idea is to install on the root logger an instance of
 * <code>GLFBridgeHandler</code> as the sole JUL handler in the system.
 *
 * @author Leonard Woo
 */
public class GLFBridgeHandler extends Handler {

  private static final String FQCN = java.util.logging.Logger.class.getName();
  private static final String UNKNOWN_LOGGER_NAME = "unknown.jul.logger";

  private static final int TRACE_LEVEL_THRESHOLD = Level.FINEST.intValue();
  private static final int DEBUG_LEVEL_THRESHOLD = Level.FINE.intValue();
  private static final int INFO_LEVEL_THRESHOLD = Level.INFO.intValue();
  private static final int WARN_LEVEL_THRESHOLD = Level.WARNING.intValue();

  /**
   * Adds a GLFBridgeHandler instance to jul's root logger.
   */
  public static void install() {
    removeHandlersForRootLogger();
    getRootLogger().addHandler(new GLFBridgeHandler());
  }

  private static java.util.logging.Logger getRootLogger() {
    return LogManager.getLogManager().getLogger("");
  }

  /**
   * Removes previously installed GLFBridgeHandler instances. See also
   * {@link #install()}.
   *
   * @throws SecurityException A <code>SecurityException</code> is thrown, if a security manager
   *   exists and if the caller does not have LoggingPermission("control").
   */
  public static void uninstall() throws SecurityException {
    java.util.logging.Logger rootLogger = getRootLogger();
    Handler[] handlers = rootLogger.getHandlers();
    for (Handler handler : handlers) {
      if (handler instanceof GLFBridgeHandler) {
        rootLogger.removeHandler(handler);
      }
    }
  }

  /**
   * Returns true if GLFBridgeHandler has been previously installed, returns false otherwise.
   *
   * @return true if GLFBridgeHandler is already installed, false other wise
   *
   */
  public static boolean isInstalled()  {
    java.util.logging.Logger rootLogger = getRootLogger();
    Handler[] handlers = rootLogger.getHandlers();
    for (Handler handler : handlers) {
      if (handler instanceof GLFBridgeHandler) {
        return true;
      }
    }
    return false;
  }

  /**
   * Delete java.util.logging default logger handler
   */
  public static void removeHandlersForRootLogger() {
    java.util.logging.Logger rootLogger = getRootLogger();
    java.util.logging.Handler[] handlers = rootLogger.getHandlers();
    for (Handler handler : handlers) {
      rootLogger.removeHandler(handler);
    }
  }

  /**
   * Initialize this handler.
   */
  public GLFBridgeHandler() {
  }

  /**
   * No-op implementation.
   */
  public void close() {
    // empty
  }

  /**
   * No-op implementation.
   */
  public void flush() {
    // empty
  }

  /**
   * Return the Logger instance that will be used for logging.
   *
   * @param record a LogRecord
   * @return an GLF logger corresponding to the record parameter's logger name
   */
  protected Logger getGLFLogger(LogRecord record) {
    String name = record.getLoggerName();
    if (name == null) {
      name = UNKNOWN_LOGGER_NAME;
    }
    return LoggerFactory.getLogger(name);
  }

  protected void callLocationAwareLogger(LocationLogger lal, LogRecord record) {
    int julLevelValue = record.getLevel().intValue();
    org.seppiko.glf.api.Level glfLevel;

    if (julLevelValue <= TRACE_LEVEL_THRESHOLD) {
      glfLevel = org.seppiko.glf.api.Level.TRACE;
    } else if (julLevelValue <= DEBUG_LEVEL_THRESHOLD) {
      glfLevel = org.seppiko.glf.api.Level.DEBUG;
    } else if (julLevelValue <= INFO_LEVEL_THRESHOLD) {
      glfLevel = org.seppiko.glf.api.Level.INFO;
    } else if (julLevelValue <= WARN_LEVEL_THRESHOLD) {
      glfLevel = org.seppiko.glf.api.Level.WARN;
    } else {
      glfLevel = org.seppiko.glf.api.Level.ERROR;
    }
    String i18nMessage = getMessageI18N(record);
    lal.log(FQCN, glfLevel, null, i18nMessage, null, record.getThrown());
  }

  protected void callPlainGLFLogger(Logger glfLogger, LogRecord record) {
    String i18nMessage = getMessageI18N(record);
    int julLevelValue = record.getLevel().intValue();
    if (julLevelValue <= TRACE_LEVEL_THRESHOLD) {
      glfLogger.trace(i18nMessage, record.getThrown());
    } else if (julLevelValue <= DEBUG_LEVEL_THRESHOLD) {
      glfLogger.debug(i18nMessage, record.getThrown());
    } else if (julLevelValue <= INFO_LEVEL_THRESHOLD) {
      glfLogger.info(i18nMessage, record.getThrown());
    } else if (julLevelValue <= WARN_LEVEL_THRESHOLD) {
      glfLogger.warn(i18nMessage, record.getThrown());
    } else {
      glfLogger.error(i18nMessage, record.getThrown());
    }
  }

  /**
   * Get the record's message, possibly via a resource bundle.
   */
  private String getMessageI18N(LogRecord record) {
    String message = record.getMessage();

    if (message == null) {
      return null;
    }

    ResourceBundle bundle = record.getResourceBundle();
    if (bundle != null) {
      try {
        message = bundle.getString(message);
      } catch (MissingResourceException ignored) {
      }
    }
    Object[] params = record.getParameters();
    if (params != null && params.length > 0) {
      try {
        message = MessageFormat.format(message, params);
      } catch (IllegalArgumentException e) {
        return message;
      }
    }
    return message;
  }

  /**
   * Publish a LogRecord.
   *
   * <p> The logging request was made initially to a Logger object, which
   * initialized the LogRecord and forwarded it here.
   *
   * <p> This handler ignores the Level attached to the LogRecord, as GLF cares
   * about discarding log statements.
   *
   * @param record Description of the log event. A null record is silently ignored
   *               and is not published.
   */
  public void publish(LogRecord record) {
    if (record == null) {
      return;
    }

    Logger glfLogger = getGLFLogger(record);
    if (record.getMessage() == null) {
      record.setMessage("");
    }
    if (glfLogger instanceof LocationLogger) {
      callLocationAwareLogger((LocationLogger) glfLogger, record);
    } else {
      callPlainGLFLogger(glfLogger, record);
    }
  }
}
