= GLF(General Logging Facade)

== 2.2.0 - 2024-03-29
. Update `org.seppiko.glf:glf-log4j2` Apache Log4j 2.23.1
. Update `org.seppiko.glf:scl-over-glf` Seppiko Commons Logging 3.2.0
. Update copyright
. Fixed bug
. Update document
. Update OSSRH to Maven Central

== 2.1.0 - 2023-05-10
. Update document
. Update `org.seppiko.glf:glf-log4j2`  Apache Log4j 2.20.0
. Update `org.seppiko.glf:scl-over-glf` Seppiko Commons Logging 3.1.0

== 2.0.0 - 2023-01-03
. Add `org.seppiko.glf:scl-over-glf`
. Add fluent logger API
. Remove static binder and factory binder
. Update copyright
. Update document

== 1.5.0 - 2022-11-19
. Update JDK17
. Update `org.seppiko.glf:glf-log4j2` Apache Log4j 2.19.0

== 1.4.1 - 2022-03-21
. Update `org.seppiko.glf:glf-log4j2` Apache Log4j 2.17.1

== 1.4.RELEASE - 2021-12-11
. Move `spring-boot-starter-glf` to this repo
. Update `org.seppiko.glf:glf-log4j2` dependency management
. Update copyright

== 1.3.RELEASE - 2020-11-24
. Update `org.seppiko.glf:glf-api` dependency management
. Change `org.seppiko.glf.event.DefaultMarkerFactory` to `org.seppiko.glf.api.MarkerFactory`
. Update `MarkerFactory` method to `static`
. Fixed GLF Log4j2 Marker bug

== 1.2.RELEASE - 2020-10-11
. Remove GLF JUL
. Add JUL to GLF
. Add JCL over GLF

== 1.1.RELEASE - 2020-10-05
. Implement GLF Log4j2

== 1.0.RELEASE - 2020-10-05
. Implement GLF API
. Implement GLF JUL - A bridge java.util.logging
. Implement Service register
