/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.log4j2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.spi.AbstractLoggerAdapter;
import org.apache.logging.log4j.spi.LoggerContext;
import org.apache.logging.log4j.util.StackLocatorUtil;
import org.seppiko.glf.api.ILoggerFactory;
import org.seppiko.glf.api.Logger;

/**
 * Log4j 2.x Logger Factory registered
 *
 * @author Leonard Woo
 */
public class Log4j2LoggerFactory extends AbstractLoggerAdapter<Logger> implements ILoggerFactory {

  private static final String FQCN = Log4j2LoggerFactory.class.getName();
  private static final String PACKAGE = "org.seppiko.glf";
  private final Log4j2MarkerFactory markerFactory;

  /**
   * constructor
   *
   * @param markerFactory Log4j2 marker factory instance
   */
  public Log4j2LoggerFactory(Log4j2MarkerFactory markerFactory) {
    this.markerFactory = markerFactory;
  }

  @Override
  protected Logger newLogger(final String name, final LoggerContext loggerContext) {
    final String key = "ROOT".equals(name) ? LogManager.ROOT_LOGGER_NAME : name;
    return new Log4j2Logger(markerFactory, loggerContext.getLogger(key), name);
  }

  @Override
  protected LoggerContext getContext() {
    final Class<?> anchor = StackLocatorUtil.getCallerClass(FQCN, PACKAGE);
    return anchor == null ? LogManager.getContext() : getContext(StackLocatorUtil.getCallerClass(anchor));
  }
}
