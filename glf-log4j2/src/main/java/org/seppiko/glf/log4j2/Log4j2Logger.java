/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.log4j2;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Supplier;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.ParameterizedMessage;
import org.apache.logging.log4j.message.SimpleMessage;
import org.apache.logging.log4j.spi.ExtendedLogger;
import org.apache.logging.log4j.Level;
import org.seppiko.glf.api.LocationLogger;
import org.seppiko.glf.api.Marker;
import org.seppiko.glf.event.LoggerEventBuilder;
import org.seppiko.glf.event.nop.NOPLoggerEventBuilder;

/**
 * Log4j 2.x Logger implementation convert
 *
 * @author Leonard Woo
 */
public class Log4j2Logger implements LocationLogger {

  public static final String FQCN = Log4j2Logger.class.getName();

  private transient ExtendedLogger logger;
  private transient Log4j2MarkerFactory markerFactory;
  private final String name;

  /**
   * constructor
   *
   * @param markerFactory Log4j2 marker factory instance
   * @param logger log4j2 extended logger instance
   * @param name log4j2 logger name
   */
  public Log4j2Logger(final Log4j2MarkerFactory markerFactory, final ExtendedLogger logger, final String name) {
    this.markerFactory = markerFactory;
    this.logger = logger;
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public LoggerEventBuilder atLevel(org.seppiko.glf.api.Level level) {
    final Level log4j2Level = getLevel(level);
    if (logger.isEnabled(log4j2Level)) {
      return new Log4j2EventBuilder(markerFactory, logger.atLevel(log4j2Level));
    }
    return NOPLoggerEventBuilder.singleton();
  }

  @Override
  public boolean isEnable(org.seppiko.glf.api.Level level) {
    return logger.isEnabled(getLevel(level));
  }

  @Override
  public boolean isEnable(org.seppiko.glf.api.Level level, Marker marker) {
    return logger.isEnabled(getLevel(level), getMarker(marker));
  }

  @Override
  public boolean isTraceEnabled() {
    return logger.isEnabled(Level.TRACE, null, null);
  }

  @Override
  public boolean isTraceEnabled(Marker marker) {
    return logger.isEnabled(Level.TRACE, getMarker(marker), null);
  }

  @Override
  public void trace(String message) {
    logger.logIfEnabled(FQCN, Level.TRACE, null, message);
  }

  @Override
  public void trace(String message, Object param) {
    logger.logIfEnabled(FQCN, Level.TRACE, null, message, param);
  }

  @Override
  public void trace(String message, Object... params) {
    logger.logIfEnabled(FQCN, Level.TRACE, null, message, params);
  }

  @Override
  public void trace(String message, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.TRACE, null, message, cause);
  }

  @Override
  public void trace(String message, Object param, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.TRACE, null, message, param, cause);
  }

  @Override
  public void trace(String message, Object[] params, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.TRACE, null, message, params, cause);
  }

  @Override
  public void trace(Marker marker, String message) {
    logger.logIfEnabled(FQCN, Level.TRACE, getMarker(marker), message);
  }

  @Override
  public void trace(Marker marker, String message, Object param) {
    logger.logIfEnabled(FQCN, Level.TRACE, getMarker(marker), message, param);
  }

  @Override
  public void trace(Marker marker, String message, Object... params) {
    logger.logIfEnabled(FQCN, Level.TRACE, getMarker(marker), message, params);
  }

  @Override
  public void trace(Marker marker, String message, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.TRACE, getMarker(marker), message, cause);
  }

  @Override
  public void trace(Marker marker, String message, Object param, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.TRACE, getMarker(marker), message, param, cause);
  }

  @Override
  public void trace(Marker marker, String message, Object[] params, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.TRACE, getMarker(marker), message, params, cause);
  }

  @Override
  public LoggerEventBuilder atTrace() {
    if (logger.isTraceEnabled()) {
      return new Log4j2EventBuilder(markerFactory, logger.atTrace());
    }
    return NOPLoggerEventBuilder.singleton();
  }

  @Override
  public boolean isDebugEnabled() {
    return logger.isEnabled(Level.DEBUG, null, null);
  }

  @Override
  public boolean isDebugEnabled(Marker marker) {
    return logger.isEnabled(Level.DEBUG, getMarker(marker), null);
  }

  @Override
  public void debug(String message) {
    logger.logIfEnabled(FQCN, Level.DEBUG, null, message);
  }

  @Override
  public void debug(String message, Object param) {
    logger.logIfEnabled(FQCN, Level.DEBUG, null, message, param);
  }

  @Override
  public void debug(String message, Object... params) {
    logger.logIfEnabled(FQCN, Level.DEBUG, null, message, params);
  }

  @Override
  public void debug(String message, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.DEBUG, null, message, cause);
  }

  @Override
  public void debug(String message, Object param, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.DEBUG, null, message, param, cause);
  }

  @Override
  public void debug(String message, Object[] params, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.DEBUG, null, message, params, cause);
  }

  @Override
  public void debug(Marker marker, String message) {
    logger.logIfEnabled(FQCN, Level.DEBUG, getMarker(marker), message);
  }

  @Override
  public void debug(Marker marker, String message, Object param) {
    logger.logIfEnabled(FQCN, Level.DEBUG,  getMarker(marker), message, param);
  }

  @Override
  public void debug(Marker marker, String message, Object... params) {
    logger.logIfEnabled(FQCN, Level.DEBUG,  getMarker(marker), message, params);
  }

  @Override
  public void debug(Marker marker, String message, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.DEBUG,  getMarker(marker), message, cause);
  }

  @Override
  public void debug(Marker marker, String message, Object param, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.DEBUG,  getMarker(marker), message, param, cause);
  }

  @Override
  public void debug(Marker marker, String message, Object[] params, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.DEBUG,  getMarker(marker), message, params, cause);
  }

  @Override
  public LoggerEventBuilder atDebug() {
    if (logger.isDebugEnabled()) {
      return new Log4j2EventBuilder(markerFactory, logger.atDebug());
    }
    return NOPLoggerEventBuilder.singleton();
  }

  @Override
  public boolean isInfoEnabled() {
    return logger.isEnabled(Level.INFO, null, null);
  }

  @Override
  public boolean isInfoEnabled(Marker marker) {
    return logger.isEnabled(Level.INFO, getMarker(marker), null);
  }

  @Override
  public void info(String message) {
    logger.logIfEnabled(FQCN, Level.INFO, null, message);
  }

  @Override
  public void info(String message, Object param) {
    logger.logIfEnabled(FQCN, Level.INFO, null, message, param);
  }

  @Override
  public void info(String message, Object... params) {
    logger.logIfEnabled(FQCN, Level.INFO, null, message, params);
  }

  @Override
  public void info(String message, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.INFO, null, message, cause);
  }

  @Override
  public void info(String message, Object param, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.INFO, null, message, param, cause);
  }

  @Override
  public void info(String message, Object[] params, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.INFO, null, message, params, cause);
  }

  @Override
  public void info(Marker marker, String message) {
    logger.logIfEnabled(FQCN, Level.INFO, getMarker(marker), message);
  }

  @Override
  public void info(Marker marker, String message, Object param) {
    logger.logIfEnabled(FQCN, Level.INFO, getMarker(marker), message, param);
  }

  @Override
  public void info(Marker marker, String message, Object... params) {
    logger.logIfEnabled(FQCN, Level.INFO, getMarker(marker), message, params);
  }

  @Override
  public void info(Marker marker, String message, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.INFO, getMarker(marker), message, cause);
  }

  @Override
  public void info(Marker marker, String message, Object param, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.INFO, getMarker(marker), message, param, cause);
  }

  @Override
  public void info(Marker marker, String message, Object[] params, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.INFO, getMarker(marker), message, params, cause);
  }

  @Override
  public LoggerEventBuilder atInfo() {
    if (logger.isInfoEnabled()) {
      return new Log4j2EventBuilder(markerFactory, logger.atInfo());
    }
    return NOPLoggerEventBuilder.singleton();
  }

  @Override
  public boolean isWarnEnabled() {
    return logger.isEnabled(Level.WARN, null, null);
  }

  @Override
  public boolean isWarnEnabled(Marker marker) {
    return logger.isEnabled(Level.WARN, getMarker(marker), null);
  }

  @Override
  public void warn(String message) {
    logger.logIfEnabled(FQCN, Level.WARN, null, message);
  }

  @Override
  public void warn(String message, Object param) {
    logger.logIfEnabled(FQCN, Level.WARN, null, message, param);
  }

  @Override
  public void warn(String message, Object... params) {
    logger.logIfEnabled(FQCN, Level.WARN, null, message, params);
  }

  @Override
  public void warn(String message, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.WARN, null, message, cause);
  }

  @Override
  public void warn(String message, Object param, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.WARN, null, message, param, cause);
  }

  @Override
  public void warn(String message, Object[] params, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.WARN, null, message, params, cause);
  }

  @Override
  public void warn(Marker marker, String message) {
    logger.logIfEnabled(FQCN, Level.WARN, getMarker(marker), message);
  }

  @Override
  public void warn(Marker marker, String message, Object param) {
    logger.logIfEnabled(FQCN, Level.WARN, getMarker(marker), message, param);
  }

  @Override
  public void warn(Marker marker, String message, Object... params) {
    logger.logIfEnabled(FQCN, Level.WARN, getMarker(marker), message, params);
  }

  @Override
  public void warn(Marker marker, String message, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.WARN, getMarker(marker), message, cause);
  }

  @Override
  public void warn(Marker marker, String message, Object param, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.WARN, getMarker(marker), message, param, cause);
  }

  @Override
  public void warn(Marker marker, String message, Object[] params, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.WARN, getMarker(marker), message, params, cause);
  }

  @Override
  public LoggerEventBuilder atWarn() {
    if (logger.isWarnEnabled()) {
      return new Log4j2EventBuilder(markerFactory, logger.atWarn());
    }
    return NOPLoggerEventBuilder.singleton();
  }

  @Override
  public boolean isErrorEnabled() {
    return logger.isEnabled(Level.ERROR, null, null);
  }

  @Override
  public boolean isErrorEnabled(Marker marker) {
    return logger.isEnabled(Level.ERROR, getMarker(marker), null);
  }

  @Override
  public void error(String message) {
    logger.logIfEnabled(FQCN, Level.ERROR, null, message);
  }

  @Override
  public void error(String message, Object param) {
    logger.logIfEnabled(FQCN, Level.ERROR, null, message, param);
  }

  @Override
  public void error(String message, Object... params) {
    logger.logIfEnabled(FQCN, Level.ERROR, null, message, params);
  }

  @Override
  public void error(String message, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.ERROR, null, message, cause);
  }

  @Override
  public void error(String message, Object param, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.ERROR, null, message, param, cause);
  }

  @Override
  public void error(String message, Object[] params, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.ERROR, null, message, params, cause);
  }

  @Override
  public void error(Marker marker, String message) {
    logger.logIfEnabled(FQCN, Level.ERROR, getMarker(marker), message);
  }

  @Override
  public void error(Marker marker, String message, Object param) {
    logger.logIfEnabled(FQCN, Level.ERROR, getMarker(marker), message, param);
  }

  @Override
  public void error(Marker marker, String message, Object... params) {
    logger.logIfEnabled(FQCN, Level.ERROR, getMarker(marker), message, params);
  }

  @Override
  public void error(Marker marker, String message, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.ERROR, getMarker(marker), message, cause);
  }

  @Override
  public void error(Marker marker, String message, Object param, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.ERROR, getMarker(marker), message, param, cause);
  }

  @Override
  public void error(Marker marker, String message, Object[] params, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.ERROR, getMarker(marker), message, params, cause);
  }

  @Override
  public LoggerEventBuilder atError() {
    if (logger.isErrorEnabled()) {
      return new Log4j2EventBuilder(markerFactory, logger.atError());
    }
    return NOPLoggerEventBuilder.singleton();
  }

  @Override
  public boolean isFatalEnabled() {
    return logger.isEnabled(Level.FATAL, null, null);
  }

  @Override
  public boolean isFatalEnabled(Marker marker) {
    return logger.isEnabled(Level.FATAL, getMarker(marker), null);
  }

  @Override
  public void fatal(String message) {
    logger.logIfEnabled(FQCN, Level.FATAL, null, message);
  }

  @Override
  public void fatal(String message, Object param) {
    logger.logIfEnabled(FQCN, Level.FATAL, null, message, param);
  }

  @Override
  public void fatal(String message, Object... params) {
    logger.logIfEnabled(FQCN, Level.FATAL, null, message, params);
  }

  @Override
  public void fatal(String message, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.FATAL, null, message, cause);
  }

  @Override
  public void fatal(String message, Object param, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.FATAL, null, message, param, cause);
  }

  @Override
  public void fatal(String message, Object[] params, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.FATAL, null, message, params, cause);
  }

  @Override
  public void fatal(Marker marker, String message) {
    logger.logIfEnabled(FQCN, Level.FATAL, getMarker(marker), message);
  }

  @Override
  public void fatal(Marker marker, String message, Object param) {
    logger.logIfEnabled(FQCN, Level.FATAL, getMarker(marker), message, param);
  }

  @Override
  public void fatal(Marker marker, String message, Object... params) {
    logger.logIfEnabled(FQCN, Level.FATAL, getMarker(marker), message, params);
  }

  @Override
  public void fatal(Marker marker, String message, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.FATAL, getMarker(marker), message, cause);
  }

  @Override
  public void fatal(Marker marker, String message, Object param, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.FATAL, getMarker(marker), message, param, cause);
  }

  @Override
  public void fatal(Marker marker, String message, Object[] params, Throwable cause) {
    logger.logIfEnabled(FQCN, Level.FATAL, getMarker(marker), message, params, cause);
  }

  @Override
  public LoggerEventBuilder atFatal() {
    if (logger.isFatalEnabled()) {
      return new Log4j2EventBuilder(markerFactory, logger.atFatal());
    }
    return NOPLoggerEventBuilder.singleton();
  }

  @Override
  public void log(String fqcn, org.seppiko.glf.api.Level level, Marker marker, String message,
      Object[] params, Throwable cause) {
    final Level log4jLevel = getLevel(level);
    final org.apache.logging.log4j.Marker log4jMarker = getMarker(marker);

    if (!logger.isEnabled(log4jLevel, log4jMarker, message, params)) {
      return;
    }
    final Message msg;
//    if (CONVERTER != null && eventLogger && marker != null && marker.contains(EVENT_MARKER)) {
//      msg = CONVERTER.convertEvent(message, params, cause);
//    } else
    if (params == null) {
      msg = new SimpleMessage(message);
    } else {
      msg = new ParameterizedMessage(message, params, cause);
      if (cause != null) {
        cause = msg.getThrowable();
      }
    }
    logger.logMessage(fqcn, log4jLevel, log4jMarker, msg, cause);
  }

  @Override
  public void log(String fqcn, org.seppiko.glf.api.Level level, Marker marker, String message,
      Supplier<?>[] paramSuppliers, Throwable cause) {
    log(fqcn, level, marker, message, getObjects(paramSuppliers), cause);
  }

  private org.apache.logging.log4j.Marker getMarker(final Marker marker) {
    return markerFactory.getLog4j2Marker(marker);
  }

  private Object[] getObjects(Supplier<?>[] suppliers) {
    if (suppliers == null) {
      return null;
    }
    return Arrays.stream(suppliers)
        .filter(Objects::nonNull)
        .map(Supplier::get)
        .toArray(Object[]::new);
  }

//  public boolean isEnabledForLevel(org.seppiko.glf.api.Level level) {
//    return logger.isEnabled(getLevel(level));
//  }

  private static Level getLevel(org.seppiko.glf.api.Level level) {
    return switch (level) {
      case TRACE -> Level.TRACE;
      case DEBUG -> Level.DEBUG;
      case INFO -> Level.INFO;
      case WARN -> Level.WARN;
      case ERROR -> Level.ERROR;
      default -> Level.FATAL;
    };
  }

}
