/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.log4j2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.logging.log4j.MarkerManager;
import org.seppiko.glf.api.IMarkerFactory;
import org.seppiko.glf.api.Marker;

/**
 * Log4j 2.x Marker implementation convert
 *
 * @author Leonard Woo
 */
public class Log4j2Marker implements Marker {

  private final IMarkerFactory factory;
  private final org.apache.logging.log4j.Marker marker;

  /**
   * constructor
   *
   * @param factory Log4j2 marker factory interface
   * @param marker Log4j2 marker instance
   */
  public Log4j2Marker(IMarkerFactory factory, org.apache.logging.log4j.Marker marker) {
    this.factory = factory;
    this.marker = marker;
  }

  /**
   * Get Log4j2 marker instance
   *
   * @return marker instance
   */
  public org.apache.logging.log4j.Marker getLog4j2Marker() {
    return marker;
  }

  @Override
  public String getName() {
    return marker.getName();
  }

  @Override
  public void add(Marker reference) {
    if (reference == null) {
      throw new IllegalArgumentException();
    }
    final Marker m = factory.getMarker(reference.getName());
    this.marker.addParents((org.apache.logging.log4j.Marker) m);
  }

  @Override
  public boolean remove(Marker reference) {
    return marker != null && this.marker.remove(MarkerManager.getMarker(marker.getName()));
  }

  @Override
  public boolean hasReferences() {
    return marker.hasParents();
  }

  @Override
  public Iterator<Marker> iterator() {
    final org.apache.logging.log4j.Marker[] log4j2Parents = this.marker.getParents();
    final List<Marker> parents = new ArrayList<>(log4j2Parents.length);
    for (final org.apache.logging.log4j.Marker m : log4j2Parents) {
      parents.add(factory.getMarker(m.getName()));
    }
    return parents.iterator();
  }

  @Override
  public boolean contains(Marker other) {
    if (marker == null) {
      throw new IllegalArgumentException();
    }
    return this.marker.isInstanceOf(marker.getName());
  }

  @Override
  public boolean contains(String name) {
    return name != null && this.marker.isInstanceOf(name);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof Log4j2Marker)) {
      return false;
    }
    final Log4j2Marker other = (Log4j2Marker) obj;
    if (marker == null) {
      if (other.marker != null) {
        return false;
      }
    } else if (!marker.equals(other.marker)) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((marker == null) ? 0 : marker.hashCode());
    return result;
  }
}
