/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.log4j2;

import org.seppiko.glf.api.ILoggerFactory;
import org.seppiko.glf.api.IMarkerFactory;
import org.seppiko.glf.spi.GLFServiceProvider;

/**
 * Log4j 2.x Service Provider registered
 *
 * @author Leonard Woo
 */
public class Log4j2ServiceProvider implements GLFServiceProvider {

  private ILoggerFactory loggerFactory;
  private Log4j2MarkerFactory markerFactory;

  @Override
  public ILoggerFactory getILoggerFactory() {
    return loggerFactory;
  }

  @Override
  public IMarkerFactory getMarkerFactory() {
    return markerFactory;
  }

  @Override
  public void initialize() {
    markerFactory = new Log4j2MarkerFactory();
    loggerFactory = new Log4j2LoggerFactory(markerFactory);
  }
}
