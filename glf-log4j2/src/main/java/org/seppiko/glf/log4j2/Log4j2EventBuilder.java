/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.log4j2;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import org.seppiko.glf.api.Marker;
import org.seppiko.glf.event.LoggerEventBuilder;

import org.apache.logging.log4j.BridgeAware;
import org.apache.logging.log4j.LogBuilder;

/**
 * Logger event builder implement with Log4j2
 *
 * @author Leonard Woo
 */
public class Log4j2EventBuilder implements LoggerEventBuilder {

  private static final String FQCN = Log4j2EventBuilder.class.getName();

  private final Log4j2MarkerFactory markerFactory;
  private final LogBuilder logBuilder;
  private final List<Object> arguments = new ArrayList<>();
  private String message = null;

  /**
   * constructor
   *
   * @param markerFactory Log4j2 marker factory instance
   * @param logBuilder log4j2 log builder instance
   */
  public Log4j2EventBuilder(final Log4j2MarkerFactory markerFactory, final LogBuilder logBuilder) {
    this.markerFactory = markerFactory;
    this.logBuilder = logBuilder;
    if (logBuilder instanceof BridgeAware) {
      ((BridgeAware) logBuilder).setEntryPoint(FQCN);
    }
  }

  @Override
  public LoggerEventBuilder marker(Marker marker) {
    logBuilder.withMarker(markerFactory.convertMarker(marker));
    return this;
  }

  @Override
  public LoggerEventBuilder message(String message) {
    this.message = message;
    return this;
  }

  @Override
  public LoggerEventBuilder withCause(Throwable cause) {
    logBuilder.withThrowable(cause);
    return this;
  }

  @Override
  public LoggerEventBuilder addParam(Supplier<?> paramSupplier) {
    arguments.add(paramSupplier.get());
    return this;
  }

  @Override
  public LoggerEventBuilder addParam(Object paramObject) {
    arguments.add(paramObject);
    return this;
  }

  @Override
  public void log() {
    logBuilder.log(message, arguments.toArray());
  }
}
