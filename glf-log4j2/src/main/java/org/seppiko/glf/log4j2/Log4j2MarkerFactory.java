/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.log4j2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.status.StatusLogger;
import org.seppiko.glf.api.IMarkerFactory;
import org.seppiko.glf.api.Marker;

/**
 * Log4j 2.x Marker factory implementation and convert
 *
 * @author Leonard Woo
 */
public class Log4j2MarkerFactory implements IMarkerFactory {

  private static final Logger LOGGER = StatusLogger.getLogger();
  private final ConcurrentMap<String, Marker> markerMap = new ConcurrentHashMap<>();

  private Marker addMarkerIfAbsent(final String name, final org.apache.logging.log4j.Marker log4jMarker) {
    final Marker marker = new Log4j2Marker(this, log4jMarker);
    final Marker existing = markerMap.putIfAbsent(name, marker);
    return existing == null ? marker : existing;
  }

  @Override
  public Marker getMarker(String name) {
    if (name == null) {
      throw new IllegalArgumentException("Marker name must not be null");
    }
    final Marker marker = markerMap.get(name);
    if (marker != null) {
      return marker;
    }
    final org.apache.logging.log4j.Marker log4j2Marker = MarkerManager.getMarker(name);
    return addMarkerIfAbsent(name, log4j2Marker);
  }

  /**
   * Get GLF Marker from marker map
   *
   * @param marker GLF Marker
   * @return GLF Marker instance
   */
  public Marker getMarker(final Marker marker) {
    if (marker == null) {
      throw new IllegalArgumentException("Marker must not be null");
    }
    final Marker m = markerMap.get(marker.getName());
    if (m != null) {
      return m;
    }
    return addMarkerIfAbsent(marker.getName(), convertMarker(marker));
  }

  org.apache.logging.log4j.Marker getLog4j2Marker(final Marker marker) {
    if (marker == null) {
      return null;
    } else if (marker instanceof Log4j2Marker) {
      return ((Log4j2Marker) marker).getLog4j2Marker();
    } else {
      return ((Log4j2Marker) getMarker(marker)).getLog4j2Marker();
    }
  }

  protected org.apache.logging.log4j.Marker convertMarker(final Marker original) {
    if (original == null) {
      throw new IllegalArgumentException("Marker must not be null");
    }
    return convertMarker(original, new ArrayList<>());
  }

  private org.apache.logging.log4j.Marker convertMarker(final Marker original,
      final Collection<Marker> visited) {
    final org.apache.logging.log4j.Marker marker = MarkerManager.getMarker(original.getName());
    if (original.hasReferences()) {
      final Iterator<Marker> it = original.iterator();
      while (it.hasNext()) {
        final Marker next = it.next();
        if (visited.contains(next)) {
          LOGGER.warn("Found a cycle in Marker [{}]. Cycle will be broken.", next.getName());
        } else {
          visited.add(next);
          marker.addParents(convertMarker(next, visited));
        }
      }
    }
    return marker;
  }

  @Override
  public boolean exists(String name) {
    return markerMap.containsKey(name);
  }

  @Override
  public boolean detachMarker(String name) {
    return false;
  }

  @Override
  public Marker getDetachedMarker(String name) {
    LOGGER.warn("Log4j2 does not support detached Markers. Returned Marker [{}] will be unchanged.", name);
    return getMarker(name);
  }
}
