/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf;

import org.junit.jupiter.api.Test;
import org.seppiko.glf.api.Level;
import org.seppiko.glf.api.LocationLogger;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;
import org.seppiko.glf.api.MarkerFactory;

/**
 * Log4j 2.x Test
 *
 * @author Leonard Woo
 */
public class LoggerTest {

  @Test
  public void loggerTest() {
    Logger logger = LoggerFactory.getLogger(LoggerTest.class);
//    logger.trace("Trace");
    logger.debug("Debug");
    logger.info("Information");
    logger.warn("Warning");
    logger.error("Error");
    logger.fatal("Fatal");
    logger.info(MarkerFactory.getMarker("TEST"), "info with marker");
    logger.atInfo().message("fluent api test").log();
  }

  @Test
  public void locationLoggerTest() {
    LocationLogger logger = (LocationLogger) LoggerFactory.getLogger("Log4j2-test");
    logger.log(
        "LOCATION-TEST",
        Level.INFO,
        MarkerFactory.getMarker("MARKER-TEST"),
        "info-test",
        null,
        new Exception("exception-test"));
  }
}
