/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.jcl;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogConfigurationException;
import org.apache.commons.logging.LogFactory;
import org.seppiko.glf.api.LocationLogger;
import org.seppiko.glf.api.Logger;
import org.seppiko.glf.api.LoggerFactory;

/**
 * GLF logging factory implementation for JCL
 *
 * @author Leonard Woo
 */
public class GLFLogFactory extends LogFactory {

  ConcurrentMap<String, Log> loggerMap;

  protected Hashtable<String, Object> attributes = new Hashtable<>();

  public GLFLogFactory() {
    this.loggerMap = new ConcurrentHashMap<>();
  }

  @Override
  public Object getAttribute(String name) {
    return attributes.get(name);
  }

  @Override
  public String[] getAttributeNames() {
    String[] names = new String[attributes.size()];
    Enumeration<String> keys = attributes.keys();
    for (int i = 0;keys.hasMoreElements(); i++) {
      names[i] = keys.nextElement();
    }
    return names;

  }

  @Override
  public Log getInstance(Class clazz) throws LogConfigurationException {
    return getInstance(clazz.getName());
  }

  @Override
  public Log getInstance(String name) throws LogConfigurationException {
    Log instance = loggerMap.get(name);
    if (instance != null) {
      return instance;
    } else {
      Log newInstance;
      Logger glfLogger = LoggerFactory.getLogger(name);
      if (glfLogger instanceof LocationLogger) {
        newInstance = new GLFLocationLog((LocationLogger) glfLogger);
      } else {
        newInstance = new GLFLog(glfLogger);
      }
      Log oldInstance = loggerMap.putIfAbsent(name, newInstance);
      return oldInstance == null ? newInstance : oldInstance;
    }
  }

  @Override
  public void release() {
    System.out.println("WARN: The method " + GLFLogFactory.class + "#release() was invoked.");
    System.out.flush();
  }

  @Override
  public void removeAttribute(String name) {
    attributes.remove(name);
  }

  @Override
  public void setAttribute(String name, Object value) {
    if (value == null) {
      attributes.remove(name);
    } else {
      attributes.put(name, value);
    }
  }
}
