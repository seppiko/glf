/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.glf.jcl;

import org.apache.commons.logging.Log;
import org.seppiko.glf.api.Logger;

/**
 * GLF logging implementation for JCL
 *
 * @author Leonard Woo
 */
public class GLFLog implements Log {

  private final transient Logger logger;

  protected String name;

  public GLFLog(Logger logger) {
    this.logger = logger;
    this.name = logger.getName();
  }

  @Override
  public void debug(Object o) {
    logger.debug(String.valueOf(o));
  }

  @Override
  public void debug(Object o, Throwable t) {
    logger.debug(String.valueOf(o), t);
  }

  @Override
  public void error(Object o) {
    logger.error(String.valueOf(o));
  }

  @Override
  public void error(Object o, Throwable t) {
    logger.error(String.valueOf(o), t);
  }

  @Override
  public void fatal(Object o) {
    logger.fatal(String.valueOf(o));
  }

  @Override
  public void fatal(Object o, Throwable t) {
    logger.fatal(String.valueOf(o), t);
  }

  @Override
  public void info(Object o) {
    logger.info(String.valueOf(o));
  }

  @Override
  public void info(Object o, Throwable t) {
    logger.info(String.valueOf(o), t);
  }

  @Override
  public boolean isDebugEnabled() {
    return logger.isDebugEnabled();
  }

  @Override
  public boolean isErrorEnabled() {
    return logger.isErrorEnabled();
  }

  @Override
  public boolean isFatalEnabled() {
    return logger.isFatalEnabled();
  }

  @Override
  public boolean isInfoEnabled() {
    return logger.isInfoEnabled();
  }

  @Override
  public boolean isTraceEnabled() {
    return logger.isTraceEnabled();
  }

  @Override
  public boolean isWarnEnabled() {
    return logger.isWarnEnabled();
  }

  @Override
  public void trace(Object o) {
    logger.trace(String.valueOf(o));
  }

  @Override
  public void trace(Object o, Throwable t) {
    logger.trace(String.valueOf(o), t);
  }

  @Override
  public void warn(Object o) {
    logger.warn(String.valueOf(o));
  }

  @Override
  public void warn(Object o, Throwable t) {
    logger.warn(String.valueOf(o), t);
  }
}
